/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps;

import data.CSGData;
import data.TeachingAssistant;
import javafx.scene.control.TableView;
import jtps.jTPS_Transaction;


/**
 *
 * @author zhaotingyi
 */
public class TAReplaceUR implements jTPS_Transaction{
    private String TAname;
    private String TAemail;
    private String newName;
    private String newEmail;
    private boolean originalUG;
    private boolean UG;
    CSGData data;
    
    public TAReplaceUR(TeachingAssistant ta, String newN, String newE, CSGData data){
        this.data = data;
        newName = newN;
        newEmail = newE;
        UG = true;
        TAname = ta.getName();
        TAemail = ta.getEmail();
        originalUG = ta.getUndergrad();
    }
    
    @Override
    public void doTransaction() {
        
        //data.replaceTAName(TAname, newName);
        data.removeTA(TAname);
        data.addTA(newName, newEmail, UG);
        
    }

    @Override
    public void undoTransaction() {
        //data.replaceTAName(newName, TAname);
        data.removeTA(newName);
        data.addTA(TAname, TAemail, originalUG);
       
    }
    
    
     
}
