/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps;

import data.CSGData;
import data.Recitation;

/**
 *
 * @author Justin Chiu
 */
public class RecReplaceUR implements jTPS_Transaction{
    private Recitation recit;
    private Recitation recit2;
    CSGData data;
    
    public RecReplaceUR(Recitation r1, Recitation r2, CSGData data){
        this.recit = r1;
        this.recit2 = r2;
        this.data = data;
    }

    @Override
    public void doTransaction() {
        data.removeRec(recit2.getSection());
        data.addRecitation(recit);
    }

    @Override
    public void undoTransaction() {
        data.removeRec(recit.getSection());
        data.addRecitation(recit2);
    }
}
