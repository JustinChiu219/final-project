/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps;

import data.CSGData;
import data.Project;
import jtps.jTPS_Transaction;

/**
 *
 * @author Justin Chiu
 */
public class ProjAdderUR implements jTPS_Transaction {

    private Project proj;
    CSGData data;
    
    public ProjAdderUR(Project project, CSGData csgData) {
        this.proj = project;
        this.data = csgData;
    }

    @Override
    public void doTransaction() {
        data.addProject(proj);
    }

    @Override
    public void undoTransaction() {
        data.removeProject(proj);
    }
    
}
