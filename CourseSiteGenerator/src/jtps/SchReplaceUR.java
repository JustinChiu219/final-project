/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps;

import data.CSGData;
import data.Schedule;
import jtps.jTPS_Transaction;

/**
 *
 * @author Justin Chiu
 */
public class SchReplaceUR implements jTPS_Transaction {
    
    private Schedule s1;
    private Schedule s2;
    CSGData data;
    
    public SchReplaceUR(Schedule schedule, Schedule schedule0, CSGData csgData) {
        this.s1 = schedule;
        this.s2 = schedule0;
        this.data = csgData;
    }

    @Override
    public void doTransaction() {
        data.removeSch(s2.getTitle());
        data.addSchedule(s1);
    }

    @Override
    public void undoTransaction() {
        data.removeSch(s1.getTitle());
        data.addSchedule(s2);
    }
    
}
