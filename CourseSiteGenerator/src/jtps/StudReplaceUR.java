/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps;

import data.CSGData;
import data.Student;

/**
 *
 * @author Justin Chiu
 */
public class StudReplaceUR implements jTPS_Transaction {
    
    private Student s;
    private Student s2;
    CSGData data;
    
    public StudReplaceUR(Student student, Student student2, CSGData csgData) {
        this.s = student;
        this.s2 = student2;
        this.data = csgData;
    }

    @Override
    public void doTransaction() {
        data.removeStud(s2.getFirst(), s2.getLast());
        data.addStudent(s);
    }

    @Override
    public void undoTransaction() {
        data.removeStud(s.getFirst(), s.getLast());
        data.addStudent(s2);
    }
    
}