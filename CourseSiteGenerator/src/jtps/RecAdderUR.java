package jtps;


import data.CSGData;
import data.Recitation;
import javafx.beans.property.SimpleStringProperty;
import jtps.jTPS_Transaction;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class RecAdderUR implements jTPS_Transaction{
    
    private Recitation recit;
    CSGData data;
    
    public RecAdderUR(Recitation r, CSGData data){
        this.recit = r;
        this.data = data;
    }

    @Override
    public void doTransaction() {
        data.addRecitation(recit);
    }

    @Override
    public void undoTransaction() {
        data.removeRec(recit.getSection());
    }
    
}
