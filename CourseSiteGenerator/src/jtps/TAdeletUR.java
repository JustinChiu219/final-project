/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps;

import data.CSGData;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import jtps.jTPS_Transaction;


/**
 *
 * @author zhaotingyi
 */
public class TAdeletUR implements jTPS_Transaction{
    
    CSGData data;
    private ArrayList<StringProperty> cellProps = new ArrayList<StringProperty>();
    private String TAname;
    private String TAemail;
    private boolean UA;
    
    public TAdeletUR(CSGData data, String TAname){
        this.data = data;
        this.TAname = TAname;
        TAemail = data.getTA(TAname).getEmail();
        UA = data.getTA(TAname).getUndergrad();
    }

    @Override
    public void doTransaction() {
        data.removeTA(TAname);
        
    }

    @Override
    public void undoTransaction() {
        data.addTA(TAname, TAemail, UA);
        
    }
    
}
