/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps;

import data.CSGData;
import data.Project;

/**
 *
 * @author Justin Chiu
 */
public class ProjDeleteUR implements jTPS_Transaction {

    private Project proj;
    CSGData data;
    
    public ProjDeleteUR(Project project, CSGData csgData) {
        this.proj = project;
        this.data = csgData;
    }

    @Override
    public void doTransaction() {
        data.removeProject(proj);
        data.deleteAllStud(proj);
    }

    @Override
    public void undoTransaction() {
        data.addProject(proj);
        data.addAllStud(proj);
    }
    
}

