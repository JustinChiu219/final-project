/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps;

import data.CSGData;
import data.Recitation;

/**
 *
 * @author Justin Chiu
 */
public class RecDeleteUR implements jTPS_Transaction{
    private Recitation recit;
    CSGData data;
    
    public RecDeleteUR(Recitation r, CSGData data){
        this.recit = r;
        this.data = data;
    }

    @Override
    public void doTransaction() {
        data.removeRecitation(recit);

    }

    @Override
    public void undoTransaction() {
        data.addRecitation(recit);
    }
}
