/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps;

import data.CSGData;
import data.Schedule;
import jtps.jTPS_Transaction;

/**
 *
 * @author Justin Chiu
 */
public class SchDeleteUR implements jTPS_Transaction {

    private Schedule schedule;
    CSGData data;
    
    public SchDeleteUR(Schedule sch, CSGData csgData) {
        this.schedule = sch;
        this.data = csgData;
    }

    @Override
    public void doTransaction() {
        data.removeSchedule(schedule);
    }

    @Override
    public void undoTransaction() {
        data.addSchedule(schedule);
    }
    
}
