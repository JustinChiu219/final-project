/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps;

import data.CSGData;
import data.Schedule;
import jtps.jTPS_Transaction;

/**
 *
 * @author Justin Chiu
 */
public class SchAdderUR implements jTPS_Transaction {
    
    private Schedule sch;
    CSGData data;
    
    public SchAdderUR(Schedule schedule, CSGData csgData) {
        this.sch = schedule;
        this.data = csgData;
    }

    @Override
    public void doTransaction() {
        data.addSchedule(sch);
    }

    @Override
    public void undoTransaction() {
        data.removeSchedule(sch);
    }
    
}
