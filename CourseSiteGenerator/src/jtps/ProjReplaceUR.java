/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps;

import data.CSGData;
import data.Project;

/**
 *
 * @author Justin Chiu
 */
public class ProjReplaceUR implements jTPS_Transaction {

    private Project proj;
    private Project proj2;
    CSGData data;
    
    public ProjReplaceUR(Project project, Project project2, CSGData csgData) {
        this.proj = project;
        this.proj2 = project2;
        this.data = csgData;
    }

    @Override
    public void doTransaction() {
        data.removeProj(proj2.getName());
        data.addProject(proj);

    }

    @Override
    public void undoTransaction() {
        data.removeProj(proj.getName());
        data.addProject(proj2);
    }
    
}
