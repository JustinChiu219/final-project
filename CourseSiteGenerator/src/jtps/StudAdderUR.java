/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps;

import data.CSGData;
import data.Student;
import jtps.jTPS_Transaction;

/**
 *
 * @author Justin Chiu
 */
public class StudAdderUR implements jTPS_Transaction {
    
    private Student s;
    CSGData data;
    
    public StudAdderUR(Student student, CSGData csgData) {
        this.s = student;
        this.data = csgData;
    }

    @Override
    public void doTransaction() {
        data.addStudent(s);
    }

    @Override
    public void undoTransaction() {
        data.removeStudent(s);
    }
    
}
