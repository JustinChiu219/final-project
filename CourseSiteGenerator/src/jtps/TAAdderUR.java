/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps;

import data.CSGData;
import java.util.regex.Pattern;
import jtps.jTPS_Transaction;


/**
 *
 * @author zhaotingyi
 */
public class TAAdderUR implements jTPS_Transaction{
    
    private String TAName;
    private String TAEmail;
    private boolean UG;
    CSGData data;
    
    public TAAdderUR(String name, String email, boolean undergrad, CSGData data){
        
        TAName = name;
        TAEmail = email;
        UG = undergrad;
        this.data = data;
    }

    @Override
    public void doTransaction() {
        data.addTA(TAName, TAEmail, true);
    }

    @Override
    public void undoTransaction() {
        data.removeTA(TAName);
    }
    
}
