/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test_bed;
import controllers.AppFileController;
import controllers.CSGWorkspaceController;
import data.CSGData;
import data.Page;
import data.Project;
import data.Recitation;
import data.Schedule;
import data.Student;
import data.TeachingAssistant;
import java.io.File;
import java.io.IOException;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author Justin Chiu
 */
public class TestSave {        
    public static void main(String[] args) throws IOException{
        CSGData data = new CSGData();
        AppFileController controller = new AppFileController(data);
        ObservableList<Page> pages;
        ObservableList<Recitation> rec;
        ObservableList<Schedule> schedules;
        ObservableList<Project> project;
        ObservableList<Student> students;
        ObservableList<TeachingAssistant> tas;
        
        pages = data.getPages();
        Page home = new Page(true, "Home", "index.html", "HomeBuilder.js");
        Page syllabus = new Page(true, "Syllabus", "syllabus.html", "SyllabusBuilder.js");
        Page schedule = new Page(true, "Schedule", "schedule.html", "ScheduleBuilder.js");
        Page hws = new Page(true, "HWs", "hws.html", "HWsBuilder.js");
        Page projects = new Page(false, "Projects", "projects.html", "ProjectsBuilder.js");
        pages.add(home);
        pages.add(syllabus);
        pages.add(schedule);
        pages.add(hws);
        pages.add(projects);

        tas = data.getTas();
        TeachingAssistant ta1 = new TeachingAssistant("Kevin Li", "kevin.li@stonybrook.edu", true);
        TeachingAssistant ta2 = new TeachingAssistant("Lauren Cheong", "lauren.cheong@stonybrook.edu", false);
        tas.add(ta1);
        tas.add(ta2);
       
        rec = data.getRec();
        Recitation r02 = new Recitation("R02", "McKenna", "Wed 3:30pm-4:23pm", "Old CS 2114", ta1, ta2);
        Recitation r05 = new Recitation("R05", "Banerjee", "Tues 5:30pm-6:23pm", "Old CS 2114", ta1, ta2);
        rec.add(r02);
        rec.add(r05);


        schedules = data.getSchedules();
        Schedule snowDay = new Schedule("Holiday", "2/9/2017", "SNOW DAY", "Day off");
        Schedule lecture3 = new Schedule("Lecture", "2/14/2017", "Lecture 3", "Event Programming");
        Schedule springBreak = new Schedule("Holiday", "3/13/2017", "Spring Break", "Day off");
        Schedule hw3 = new Schedule("HW", "3/27/2017", "HW3", "UML");
        schedules.add(snowDay);
        schedules.add(lecture3);
        schedules.add(springBreak);
        schedules.add(hw3);


        project = data.getProjects();
        Project atomic = new Project("Atomic Comics", "552211", "ffffff", "http://atomicomic.com");
        Project c4 = new Project("C4 Comics", "235399", "ffffff", "https://c4-comics.appspot.com");
        project.add(atomic);
        project.add(c4);


        students = data.getStudents();
        Student beau = new Student("Beau", "Brummell", "Atomic Comics", "Lead Designer");
        Student jane = new Student("Jane", "Doe", "C4 Comics", "Lead Programmer");
        Student noonian = new Student("Noonian", "Soong", "Atomic Comics", "Data Designer");
        students.add(beau);
        students.add(jane);
        students.add(noonian);

        controller.handleSaveRequest("work/" + "SiteSaveTest" + ".json");

    }

}
