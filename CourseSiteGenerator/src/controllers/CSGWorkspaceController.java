/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import jtps.StudAdderUR;
import jtps.ProjAdderUR;
import jtps.SchReplaceUR;
import jtps.SchDeleteUR;
import jtps.SchAdderUR;
import data.CSGData;
import data.Page;
import data.Project;
import data.Recitation;
import data.Schedule;
import data.Student;
import data.TeachingAssistant;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.regex.Pattern;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import static javafx.scene.paint.Color.WHITE;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.json.stream.JsonParsingException;
import jtps.ProjDeleteUR;
import jtps.ProjReplaceUR;
import jtps.RecAdderUR;
import jtps.RecDeleteUR;
import jtps.RecReplaceUR;
import jtps.StudDeleteUR;
import jtps.StudReplaceUR;
import jtps.TAAdderUR;
import jtps.TAReplaceUR;
import jtps.TAdeletUR;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import org.apache.commons.io.FileUtils;
import view.AppMessageDialogSingleton;
import view.AppYesNoCancelDialogSingleton;

/**
 *
 * @author Justin Chiu
 */
public class CSGWorkspaceController implements Initializable {
    
    ResourceBundle bundle;
    
    @FXML
    private AnchorPane workspace;
    
    @FXML
    private Label label;
    
    @FXML
    private TextField cd_title;
    
    @FXML
    private TextField cd_instr_name;
    
    @FXML
    private TextField cd_instr_home;
    
    @FXML
    private Label cd_directory;
    
    @FXML
    private Label cd_temp_dir;
    
    @FXML
    private ComboBox cd_semester;
    
    @FXML
    private ComboBox cd_subject;
    
    @FXML
    private ComboBox cd_number;
    
    @FXML
    private ComboBox cd_year;
    
    @FXML
    private ComboBox cd_style;
    
    @FXML
    private TableView<Page> cd_table;
    
    @FXML
    private TableColumn<Page, SimpleBooleanProperty> cd_use_col;
    
    @FXML
    private TableColumn<Page, SimpleStringProperty> cd_navbar_col;
    
    @FXML
    private TableColumn<Page, SimpleStringProperty> cd_file_col;
    
    @FXML
    private TableColumn<Page, SimpleStringProperty> cd_script_col;
    
    @FXML
    private ComboBox start_time;
    
    @FXML
    private ComboBox end_time;
    
    @FXML
    private GridPane ta_grid;
    
    @FXML
    private TextField ta_name;
    
    @FXML
    private TextField ta_email;
    
    @FXML
    private Button ta_add;
    
    @FXML
    private Button ta_delete;
    
    @FXML
    private TableView<Recitation> recitation_table;
    
    @FXML
    private TableColumn<Recitation, SimpleStringProperty> section_col;
    
    @FXML
    private TableColumn<Recitation, SimpleStringProperty> instr_col;
    
    @FXML
    private TableColumn<Recitation, SimpleStringProperty> day_time_col;
    
    @FXML
    private TableColumn<Recitation, SimpleStringProperty> loc_col;
    
    @FXML
    private TableColumn<Recitation, SimpleStringProperty> ta1_col;
    
    @FXML
    private TableColumn<Recitation, SimpleStringProperty> ta2_col;
    
    @FXML
    private TextField rec_date_time;
    
    @FXML
    private TextField rec_loc;
    
    @FXML
    private TextField rec_instr;
    
    @FXML
    private TextField rec_section;
    
    @FXML
    private ComboBox rec_ta1;
    
    @FXML
    private ComboBox rec_ta2;
    
    @FXML
    private TextField title_in;
    
    @FXML
    private TextField topic_in;
    
    @FXML
    private Button add_sch;
    
    @FXML
    private Button clear_sch;
    
    @FXML
    private TextField time_in;
    
    @FXML
    private TextField criteria_in;
    
    @FXML
    private TextField link_in;
    
    @FXML
    private ComboBox type_in;
    
    @FXML
    private DatePicker date_in;
    
    @FXML
    private TableView<Schedule> sch_table;
    
    @FXML
    private TableColumn<Schedule, SimpleStringProperty> sch_type;
    
    @FXML
    private TableColumn<Schedule, SimpleStringProperty> sch_date;
    
    @FXML
    private TableColumn<Schedule, SimpleStringProperty> sch_title;
    
    @FXML
    private TableColumn<Schedule, SimpleStringProperty> sch_topic;
    
    @FXML
    private ColorPicker proj_color;
    
    @FXML
    private ColorPicker proj_text_color;
    
    @FXML
    private TableView<Project> proj_table;
    
    @FXML
    private TableColumn<Project, SimpleStringProperty> name_col;
    
    @FXML
    private TableColumn<Project, SimpleStringProperty> color_col;
    
    @FXML
    private TableColumn<Project, SimpleStringProperty> text_color_col;
    
    @FXML
    private TableColumn<Project, SimpleStringProperty> link_col;
    
    @FXML
    private TextField proj_name;
    
    @FXML
    private TextField proj_link;
    
    @FXML
    private TextField stud_fn;
    
    @FXML
    private TextField stud_ln;
    
    @FXML
    private ComboBox<Project> stud_team;
    
    @FXML
    private TextField stud_role;

    @FXML
    private TableView<Student> students_table;
    
    @FXML
    private TableColumn<Student, SimpleStringProperty> stu_first;
    
    @FXML
    private TableColumn<Student, SimpleStringProperty> stu_last;
    
    @FXML
    private TableColumn<Student, SimpleStringProperty> stu_team;
    
    @FXML
    private TableColumn<Student, SimpleStringProperty> stu_role;
    
    @FXML
    private TableView<TeachingAssistant> ta_table;
    
    @FXML
    private TableColumn<TeachingAssistant, SimpleBooleanProperty> undergrad_column;
    
    @FXML
    private TableColumn<TeachingAssistant, SimpleStringProperty> name_column;
    
    @FXML
    private TableColumn<TeachingAssistant, SimpleStringProperty> email_column;
    
    @FXML
    private DatePicker cal_start;
    
    @FXML
    private DatePicker cal_end;
    
    @FXML
    private ImageView ban_img;
    
    @FXML
    private ImageView left_img;
    
    @FXML
    private ImageView right_img;
    
    CSGData csgData;
    AppFileController fileController;
    ObservableList<Page> pages;
    ObservableList<Recitation> rec;
    ObservableList<Schedule> schedules;
    ObservableList<Project> project;
    ObservableList<Student> students;
    ObservableList<TeachingAssistant> tas;
    ObservableList<String> types = 
        FXCollections.observableArrayList(
            "HWs",
            "Recitation",
            "Lecture",
            "Reference",
            "Holiday"
        );

    
    String holdTAName = "";
    String holdTAEmail = "";
    
    String holdRecSection = "";
    String holdRecInstr = "";
    String holdDayTime = "";
    String holdRecLocation = "";
    TeachingAssistant holdRecTA1;
    TeachingAssistant holdRecTA2;
    
    String holdType = "";
    String holdDate = "";
    String holdTime = "";
    String holdTitle = "";
    String holdTopic = "";
    String holdLink = "";
    String holdCriteria = "";
    
    String holdName = "";
    String holdColor = "";
    String holdTextColor = "";
    String holdProjLink = "";
    
    String holdFirst = "";
    String holdLast = "";
    String holdTeam = "";
    String holdRole = "";
    
    
    boolean add = true;
    boolean addRec = true;
    boolean addSch = true;
    boolean addProj = true;
    boolean addStud = true;
    
    File toCopy;
    File toExport;
    File selectExport;
    File toReplace;
    
    static jTPS jTPS = new jTPS();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //need to change to japanese later
        this.bundle = ResourceBundle.getBundle("rsc.bundles.app_properties");
        csgData = new CSGData();
        fileController = new AppFileController(csgData);
        loadWorkspace();

        type_in.getItems().setAll(types);
        
        
        cal_start.setValue(null);
        cal_end.setValue(null);
        
        csgData.setCd_subject(cd_subject.getPromptText());
        csgData.setCd_number(cd_number.getPromptText());
        csgData.setCd_semester(cd_semester.getPromptText());
        csgData.setCd_year(cd_year.getPromptText());
        csgData.setCd_directory(cd_directory.getText());
        csgData.setCd_temp_dir(cd_temp_dir.getText());
        csgData.setCd_style(cd_style.getPromptText());
        
        pages = csgData.getPages();
        Page home = new Page(true, "Home", "index.html", "HomeBuilder.js");
        Page syllabus = new Page(true, "Syllabus", "syllabus.html", "SyllabusBuilder.js");
        Page schedule = new Page(true, "Schedule", "schedule.html", "ScheduleBuilder.js");
        Page hws = new Page(true, "HWs", "hws.html", "HWsBuilder.js");
        Page projects = new Page(true, "Projects", "projects.html", "ProjectsBuilder.js");
        pages.add(home);
        pages.add(syllabus);
        pages.add(schedule);
        pages.add(hws);
        pages.add(projects);
        
        cd_use_col.setCellValueFactory(new PropertyValueFactory<Page, SimpleBooleanProperty>("use"));        
        cd_navbar_col.setCellValueFactory(new PropertyValueFactory<Page, SimpleStringProperty>("navbar"));
        cd_file_col.setCellValueFactory(new PropertyValueFactory<Page, SimpleStringProperty>("fileName"));
        cd_script_col.setCellValueFactory(new PropertyValueFactory<Page, SimpleStringProperty>("script"));
        
        cd_table.getItems().setAll(pages);
        
        tas = csgData.getTas();
        
        
        undergrad_column.setCellValueFactory(new PropertyValueFactory<TeachingAssistant, SimpleBooleanProperty>("undergrad"));        
        name_column.setCellValueFactory(new PropertyValueFactory<TeachingAssistant, SimpleStringProperty>("name"));
        email_column.setCellValueFactory(new PropertyValueFactory<TeachingAssistant, SimpleStringProperty>("email"));
        
        ta_table.getItems().setAll(tas);
        
        rec = csgData.getRec();
        
        
        section_col.setCellValueFactory(new PropertyValueFactory<Recitation, SimpleStringProperty>("section"));
        instr_col.setCellValueFactory(new PropertyValueFactory<Recitation, SimpleStringProperty>("instructor"));
        day_time_col.setCellValueFactory(new PropertyValueFactory<Recitation, SimpleStringProperty>("day_time"));
        loc_col.setCellValueFactory(new PropertyValueFactory<Recitation, SimpleStringProperty>("location"));
        ta1_col.setCellValueFactory(new PropertyValueFactory<Recitation, SimpleStringProperty>("ta1"));
        ta2_col.setCellValueFactory(new PropertyValueFactory<Recitation, SimpleStringProperty>("ta2"));
        
        recitation_table.getItems().setAll(rec);
        
        schedules = csgData.getSchedules();
        /*
        Schedule snowDay = new Schedule("Holiday", "2/9/2017", "SNOW DAY", "Day off");
        Schedule lecture3 = new Schedule("Lecture", "2/14/2017", "Lecture 3", "Event Programming");
        Schedule springBreak = new Schedule("Holiday", "3/13/2017", "Spring Break", "Day off");
        Schedule hw3 = new Schedule("HW", "3/27/2017", "HW3", "UML");
        
        schedules.add(snowDay);
        schedules.add(lecture3);
        schedules.add(springBreak);
        schedules.add(hw3);
        */
        
        
        sch_type.setCellValueFactory(new PropertyValueFactory<Schedule, SimpleStringProperty>("type"));
        sch_date.setCellValueFactory(new PropertyValueFactory<Schedule, SimpleStringProperty>("date"));
        sch_title.setCellValueFactory(new PropertyValueFactory<Schedule, SimpleStringProperty>("title"));
        sch_topic.setCellValueFactory(new PropertyValueFactory<Schedule, SimpleStringProperty>("topic"));
        
        sch_table.getItems().setAll(schedules);
        
        project = csgData.getProjects();
        
        
        name_col.setCellValueFactory(new PropertyValueFactory<Project, SimpleStringProperty>("name"));
        color_col.setCellValueFactory(new PropertyValueFactory<Project, SimpleStringProperty>("color"));
        text_color_col.setCellValueFactory(new PropertyValueFactory<Project, SimpleStringProperty>("text_color"));
        link_col.setCellValueFactory(new PropertyValueFactory<Project, SimpleStringProperty>("link"));
        
        proj_table.getItems().setAll(project);
        
        students = csgData.getStudents();
        
        stu_first.setCellValueFactory(new PropertyValueFactory<Student, SimpleStringProperty>("first"));
        stu_last.setCellValueFactory(new PropertyValueFactory<Student, SimpleStringProperty>("last"));
        stu_team.setCellValueFactory(new PropertyValueFactory<Student, SimpleStringProperty>("team"));
        stu_role.setCellValueFactory(new PropertyValueFactory<Student, SimpleStringProperty>("role"));
        
        students_table.getItems().setAll(students);
        updateAll();

    }
    
    @FXML
    private void handleChangeCourseDirectory(ActionEvent event) {
        csgData.setCd_title(cd_title.getText());
        
    }
    
    @FXML
    private void handleAddTA(ActionEvent event) {
        String name = ta_name.getText();
        String email = ta_email.getText();
        boolean undergrad_default = false;
        if (add == true){
            // DID THE USER NEGLECT TO PROVIDE A TA NAME?
            if (name.isEmpty()) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(bundle.getString("missing_ta_name"));            
            }
            // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
            else if (email.isEmpty()) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(bundle.getString("missing_ta_email"));                        
            }
            // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?

            else if (csgData.containsTA(name, email)) {

                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show(bundle.getString("ta_not_unique"));

            }
        }
        if (add == false){
            // DID THE USER NEGLECT TO PROVIDE A TA NAME?
            if (name.isEmpty()) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(bundle.getString("missing_ta_name"));            
            }
            // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
            else if (email.isEmpty()) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(bundle.getString("missing_ta_email"));                        
            }
            
            if (!(holdTAName.equals(ta_name.getText())) && holdTAEmail.equals(ta_email.getText())){
                // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
                if (csgData.containsTAName(ta_name.getText())) {
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show(bundle.getString("update_error"));                                    
                }
            }
            if (holdTAName.equals(ta_name.getText()) && !(holdTAEmail.equals(ta_email.getText()))){
                // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
                if (csgData.containsTAEmail(ta_email.getText())) {
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show(bundle.getString("update_error"));                                    
                }
            }

            if (!(holdTAName.equals(ta_name.getText())) && !(holdTAEmail.equals(ta_email.getText()))){
                String test = ta_email.getText();
                // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
                if (csgData.containsTA(ta_name.getText(), ta_email.getText())) {
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show(bundle.getString("update_error"));                                    
                }
            }
            
        }
        if (!Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$").matcher(email).matches()){
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(bundle.getString("invalid_ta_email"));
        }
        if (add == true){
            jTPS_Transaction addUR = new TAAdderUR(name, email, undergrad_default, csgData);
            jTPS.addTransaction(addUR);
            updateAll();
            ta_name.requestFocus();
            ta_add.setText("Add");
        }
        else{
            changeExistTA();
            add = true;
            updateAll();
            ta_name.requestFocus();
            ta_add.setText("Add");
        }
    }
    
    public void changeExistTA(){
        Object selectedItem = ta_table.getSelectionModel().getSelectedItem();
        TeachingAssistant ta = (TeachingAssistant)selectedItem;
        String newName = ta_name.getText();
        String newEmail = ta_email.getText();
        jTPS_Transaction replaceTAUR = new TAReplaceUR(ta, newName, newEmail, csgData);
        jTPS.addTransaction(replaceTAUR);
    }
    
    @FXML
    private void handleDeleteTA(ActionEvent event) {
        // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = ta_table.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                // GET THE TA AND REMOVE IT
                TeachingAssistant ta = (TeachingAssistant)selectedItem;
                String taName = ta.getName();
                
                jTPS_Transaction deletUR = new TAdeletUR(csgData, taName);
                jTPS.addTransaction(deletUR);
                ta_table.getItems().setAll(tas);
                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
                // WE'VE CHANGED STUFF
                //markWorkAsEdited();
                add = true;
                ta_add.setText("Add");
                updateAll();
            }
    }
    
    @FXML
    public void loadTAtotext(){
        ta_add.setText("Update");
        Object selectedItem = ta_table.getSelectionModel().getSelectedItem();
        if(selectedItem != null){
            TeachingAssistant ta = (TeachingAssistant)selectedItem;
            String name = ta.getName();
            String email = ta.getEmail();
            ta_name.setText(name);
            ta_email.setText(email);
            holdTAName = name;
            holdTAEmail = email;
            add = false;
        }
    }
    
    @FXML
    public void loadRecToText(){
        Object selectedItem = recitation_table.getSelectionModel().getSelectedItem();
        if(selectedItem != null){
            Recitation recit = (Recitation)selectedItem;
            String section = recit.getSection();
            String recInstructor = recit.getInstructor();
            String dayTime = recit.getDay_time();
            String recLocation = recit.getLocation();
            TeachingAssistant recTA1 = recit.getTa1();
            TeachingAssistant recTA2 = recit.getTa2();
            rec_section.setText(section);
            rec_instr.setText(recInstructor);
            rec_date_time.setText(dayTime);
            rec_loc.setText(recLocation);
            rec_ta1.setValue(recTA1);
            rec_ta2.setValue(recTA2);
            holdRecSection = section;
            holdRecInstr = recInstructor;
            holdDayTime = dayTime;
            holdRecLocation = recLocation;
            holdRecTA1 = recTA1;
            holdRecTA2 = recTA2;
            addRec = false;
        }
    }
    
    @FXML
    private void handleUndo(ActionEvent event){
        jTPS.undoTransaction();
        updateAll();
    }
    
    @FXML
    private void handleRedo(ActionEvent event){
        jTPS.doTransaction();
        updateAll();
    }
    
    @FXML
    private void handleClearField(ActionEvent event) {
        ta_name.clear();
        ta_email.clear();
    }
    
    
    public void updateAll(){
        ta_table.getItems().setAll(tas);
        rec_ta1.getItems().setAll(tas);
        rec_ta2.getItems().setAll(tas);
        recitation_table.getItems().setAll(rec);
        ta_name.clear();
        ta_email.clear();
        clearRecForm();
        clearSchForm();
        sch_table.getItems().setAll(schedules);
        proj_table.getItems().setAll(project);
        clearProjForm();
        students_table.getItems().setAll(students);
        clearStudForm();
        stud_team.getItems().setAll(project);
    }
    
    @FXML
    private void handleDeleteRec(ActionEvent event) {
        Object selectedItem = recitation_table.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                // GET THE TA AND REMOVE IT
                Recitation recit = (Recitation)selectedItem;
                //String taName = ta.getName();
                jTPS_Transaction deleteUR = new RecDeleteUR(recit, csgData);
                jTPS.addTransaction(deleteUR);
                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
                // WE'VE CHANGED STUFF
                //markWorkAsEdited();
                updateAll();
            }
        
    }
    
    @FXML
    private void handleAddRec(ActionEvent event) {
        String section = rec_section.getText();
        String recInstructor = rec_instr.getText();
        String dayTime = rec_date_time.getText();
        String recLocation = rec_loc.getText();
        TeachingAssistant TA1 = (TeachingAssistant) rec_ta1.getValue();
        TeachingAssistant TA2 = (TeachingAssistant) rec_ta2.getValue();
        String recTA1 = TA1.getName();
        String recTA2 = TA2.getName();
        
        if(section.isEmpty()){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(bundle.getString("emptyRec"));
        }
        
        else if(recInstructor.isEmpty()){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(bundle.getString("emptyRec"));
        }
        
        else if(dayTime.isEmpty()){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(bundle.getString("emptyRec"));
        }
        
        else if(recLocation.isEmpty()){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(bundle.getString("emptyRec"));
        }
        
        else if(recTA1.isEmpty()){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(bundle.getString("emptyRec"));
        }
        
        else if(recTA2.isEmpty()){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(bundle.getString("emptyRec"));
        }
        
        else if(csgData.containsRecSection(section) && !section.equals(holdRecSection)){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(bundle.getString("dupRecSections"));
        }
        
        else if(recTA1.equals(recTA2)){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(bundle.getString("dupTAs"));
        }
        else{
            if(addRec == true){
                jTPS_Transaction recAddUR = new RecAdderUR(new Recitation(section, recInstructor, dayTime, recLocation, TA1, TA2), csgData);
                jTPS.addTransaction(recAddUR);
                updateAll();
                clearRecForm();
            }

            else{
                jTPS_Transaction recUpdateUR = new RecReplaceUR(new Recitation(section, recInstructor, dayTime, recLocation, TA1, TA2), new Recitation(holdRecSection, holdRecInstr, holdDayTime, holdRecLocation, holdRecTA1, holdRecTA2), csgData);
                jTPS.addTransaction(recUpdateUR);
                updateAll();
                clearRecForm();
                addRec = true;
            }
        }
    }
    
    @FXML
    private void handleClearRec(ActionEvent event) {
        rec_section.clear();
        rec_instr.clear();
        rec_date_time.clear();
        rec_loc.clear();
        rec_ta1.setValue("");
        rec_ta2.setValue("");
        rec_section.requestFocus();
    }
    
    private void clearRecForm(){
        rec_section.clear();
        rec_instr.clear();
        rec_date_time.clear();
        rec_loc.clear();
        rec_ta1.setValue("");
        rec_ta2.setValue("");
        rec_section.requestFocus();
    }
    
    @FXML
    private void handleAddSchedule(ActionEvent event){
        String s_type = (String) type_in.getValue();
        if(date_in.getValue() == null){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(bundle.getString("emptyRec"));
        }
        else{
            String s_date = date_in.getValue().toString();
            String s_time = time_in.getText();
            String s_title = title_in.getText();
            String s_topic = topic_in.getText();
            String s_link = link_in.getText();
            String s_criteria = criteria_in.getText();

            if(s_type.isEmpty()){
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(bundle.getString("emptyRec"));
            }

            else if(s_date.isEmpty()){
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(bundle.getString("emptyRec"));
            }

            else if(s_time.isEmpty()){
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(bundle.getString("emptyRec"));
            }

            else if(s_title.isEmpty()){
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(bundle.getString("emptyRec"));
            }

            else if(s_topic.isEmpty()){
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(bundle.getString("emptyRec"));
            }

            else if(s_link.isEmpty()){
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(bundle.getString("emptyRec"));
            }

            else if(s_criteria.isEmpty()){
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(bundle.getString("emptyRec"));
            }

            else if(csgData.containsSchTitle(s_title) && !s_title.equals(holdTitle)){
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(bundle.getString("dupSchTitles"));
            }

            else{
                if(addSch == true){
                    jTPS_Transaction schAddUR = new SchAdderUR(new Schedule(s_type, s_date, s_time, s_title, s_topic, s_link, s_criteria), csgData);
                    jTPS.addTransaction(schAddUR);
                    updateAll();
                    clearSchForm();
                }
                else{
                    jTPS_Transaction schUpdateUR = new SchReplaceUR(new Schedule(s_type, s_date, s_time, s_title, s_topic, s_link, s_criteria), new Schedule(holdType, holdDate, holdTime, holdTitle, holdTopic, holdLink, holdCriteria), csgData);
                    jTPS.addTransaction(schUpdateUR);
                    updateAll();
                    clearSchForm();
                    addSch = true;
                }
            }
        }
    }
    
    @FXML
    private void handleDeleteSchedule(ActionEvent event){
        Object selectedItem = sch_table.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                // GET THE TA AND REMOVE IT
                Schedule sch = (Schedule)selectedItem;
                //String taName = ta.getName();
                jTPS_Transaction deleteUR = new SchDeleteUR(sch, csgData);
                jTPS.addTransaction(deleteUR);
                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
                // WE'VE CHANGED STUFF
                //markWorkAsEdited();
                updateAll();
            }
    }
    
    @FXML
    private void handleClearSchedule(ActionEvent event){
        type_in.setValue("");
        date_in.setValue(null);
        time_in.clear();
        title_in.clear();
        topic_in.clear();
        link_in.clear();
        criteria_in.clear();
    }
    
    @FXML
    public void loadSchToText(){
        Object selectedItem = sch_table.getSelectionModel().getSelectedItem();
        if(selectedItem != null){
            Schedule sch = (Schedule)selectedItem;
            String type = sch.getType();
            String date = sch.getDate();
            String time = sch.getTime();
            String title = sch.getTitle();
            String topic = sch.getTopic();
            String link = sch.getLink();
            String criteria = sch.getCriteria();
            type_in.setValue(type);
            LocalDate localDate = LocalDate.parse(date);
            date_in.setValue(localDate);
            time_in.setText(time);
            title_in.setText(title);
            topic_in.setText(topic);
            link_in.setText(link);
            criteria_in.setText(criteria);
            
            holdType = type;
            holdDate = date;
            holdTime = time;
            holdTitle = title;
            holdTopic = topic;
            holdLink = link;
            holdCriteria = criteria;
            addSch = false;
        }
    }
    
    @FXML
    public void loadProjToText(){
        Object selectedItem = proj_table.getSelectionModel().getSelectedItem();
        if(selectedItem != null){
            Project p = (Project)selectedItem;
            String name = p.getName();
            
            String c = p.getColor();
            
            int red = Integer.parseInt(c.substring(0, 2), 16);
            int green = Integer.parseInt(c.substring(2, 4), 16);
            int blue = Integer.parseInt(c.substring(4, 6), 16);
            Color col = Color.rgb(red, green, blue);
            
            String c2 = p.getText_color();
            red = Integer.parseInt(c2.substring(0, 2), 16);
            green = Integer.parseInt(c2.substring(2, 4), 16);
            blue = Integer.parseInt(c2.substring(4, 6), 16);
            Color textCol = Color.rgb(red, green, blue);
            String link = p.getLink();
            
            proj_name.setText(name);
            proj_color.setValue(col);
            proj_text_color.setValue(textCol);
            proj_link.setText(link);
            
            holdName = name;
            holdColor = c;
            holdTextColor = c2;
            holdProjLink = link;
            addProj = false;
        }
        
    }
    
    @FXML
    public void loadStudToText(){
        Object selectedItem = students_table.getSelectionModel().getSelectedItem();
        if(selectedItem != null){
            Student s = (Student)selectedItem;
            String first = s.getFirst();
            String last = s.getLast();
            String team = s.getTeam();
            String role = s.getRole();
            Project p = csgData.getProject(team);
            
            stud_fn.setText(first);
            stud_ln.setText(last);
            stud_team.setValue(p);
            stud_role.setText(role);
            
            
            holdFirst = first;
            holdLast = last;
            holdTeam = team;
            holdRole = role;
            addStud = false;
        }
        
    }
    
    @FXML
    private void handleAddProject(ActionEvent event){
        String name = proj_name.getText();
        String c = proj_color.getValue().toString();
        c = c.substring(2, 8);
        String c2 = proj_text_color.getValue().toString();
        c2 = c2.substring(2, 8);
        String link = proj_link.getText();
        
        if(name.isEmpty()){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(bundle.getString("emptyRec"));
        }

        else if(link.isEmpty()){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(bundle.getString("emptyRec"));
        }
        
        else if(csgData.containsProjName(name) && !name.equals(holdName)){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(bundle.getString("projName"));
        }
        
        else{
            if(addProj == true){
                jTPS_Transaction projAddUR = new ProjAdderUR(new Project(name, c, c2, link), csgData);
                jTPS.addTransaction(projAddUR);
                updateAll();
                clearProjForm();
            }
            else{
                jTPS_Transaction projReplaceUR = new ProjReplaceUR(new Project(name, c, c2, link), new Project(holdName, holdColor, holdTextColor, holdProjLink), csgData);
                jTPS.addTransaction(projReplaceUR);
                updateAll();
                clearProjForm();
                addProj = true;
            }
        }
    }
    
    @FXML
    private void handleDeleteProject(ActionEvent event){
        Object selectedItem = proj_table.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                // GET THE TA AND REMOVE IT
                Project p = (Project)selectedItem;
                //String taName = ta.getName();
                jTPS_Transaction deleteUR = new ProjDeleteUR(p, csgData);
                jTPS.addTransaction(deleteUR);
                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
                // WE'VE CHANGED STUFF
                //markWorkAsEdited();
                updateAll();
            }
    }
    
    @FXML
    private void handleClearProject(ActionEvent event){
        proj_name.setText("");
        proj_color.setValue(WHITE);
        proj_text_color.setValue(WHITE);
        proj_link.setText("");
    }
    
    @FXML
    private void handleAddStudent(ActionEvent event){
        String first = stud_fn.getText();
        String last = stud_ln.getText();
        Project p = (Project) stud_team.getValue();
        if(p == null){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(bundle.getString("emptyRec"));
        }
        else{
            String team = p.getName();
            String role = stud_role.getText();

            if(first.isEmpty()){
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(bundle.getString("emptyRec"));
            }

            else if(last.isEmpty()){
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(bundle.getString("emptyRec"));
            }

            else if(team.isEmpty()){
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(bundle.getString("emptyRec"));
            }

            else if(role.isEmpty()){
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(bundle.getString("emptyRec"));
            }

            else if(csgData.containsStudName(first, last) && !(first.equals(holdFirst) && last.equals(holdLast))){
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(bundle.getString("studName"));
            }

            else{
                if(addStud == true){
                    jTPS_Transaction studAddUR = new StudAdderUR(new Student(first, last, team, role), csgData);
                    jTPS.addTransaction(studAddUR);
                    updateAll();
                    clearProjForm();
                }
                else{
                    jTPS_Transaction studReplaceUR = new StudReplaceUR(new Student(first, last, team, role), new Student(holdFirst, holdLast, holdTeam, holdRole), csgData);
                    jTPS.addTransaction(studReplaceUR);
                    updateAll();
                    clearStudForm();
                    addStud = true;
                }
            }
        }
    }
    
    @FXML
    private void handleDeleteStudent(ActionEvent event){
        Object selectedItem = students_table.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                // GET THE TA AND REMOVE IT
                Student s = (Student)selectedItem;
                //String taName = ta.getName();
                jTPS_Transaction deleteUR = new StudDeleteUR(s, csgData);
                jTPS.addTransaction(deleteUR);
                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
                // WE'VE CHANGED STUFF
                //markWorkAsEdited();
                updateAll();
            }
    }
    
    @FXML
    private void handleClearStudent(ActionEvent event){
        stud_fn.setText("");
        stud_ln.setText("");
        stud_team.setValue(null);
        stud_role.setText("");
    }
    
    private void clearProjForm(){
        proj_name.setText("");
        proj_color.setValue(WHITE);
        proj_text_color.setValue(WHITE);
        proj_link.setText("");
    }
    
    
    @FXML
    private void handleNew(ActionEvent event) throws IOException {
        List<String> choices = new ArrayList<>();
        choices.add("English");
        choices.add("Japanese");


        ChoiceDialog<String> dialog = new ChoiceDialog<>("English", choices);
        dialog.setTitle("Choose a Language");
        dialog.setHeaderText("Select a language below");
        dialog.setContentText("Language:");

        // Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            String lang = result.get();
            if (lang.equals("English")){
                ResourceBundle bundle = ResourceBundle.getBundle("rsc.bundles.app_properties", Locale.getDefault());
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CSGWorkspace.fxml"), bundle);
                loader.setLocation(getClass().getResource("/view/CSGWorkspace.fxml"));
                Parent root = loader.load();
                Scene scene = new Scene(root);
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.setScene(scene);
                
                AppMessageDialogSingleton messageDialog = AppMessageDialogSingleton.getSingleton();
                messageDialog.init(app_stage);
                AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
                yesNoDialog.init(app_stage);
                
                app_stage.show();
            }
            else {
                ResourceBundle bundle = ResourceBundle.getBundle("rsc.bundles.app_properties_ja", Locale.JAPAN);
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CSGWorkspace.fxml"), bundle);
                loader.setLocation(getClass().getResource("/view/CSGWorkspace.fxml"));
                Parent root = loader.load();
                Scene scene = new Scene(root);
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.setScene(scene);
                app_stage.show();
            }
            
        }
    }
    
    @FXML
    private void handleLoad(ActionEvent event) {
        FileChooser fc = new FileChooser();
        File f = new File("work");
        if (!f.isDirectory()) {
            if (!f.mkdir())
                f.mkdirs();
        }
        fc.setTitle("Load Workspace");
        fc.setInitialDirectory(f);
        File file = fc.showOpenDialog(workspace.getScene().getWindow());
        if (file != null) {
            try {
                fileController.handleLoadRequest(file.getPath());
                loadWorkspace();
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText(bundle.getString("successful_load"));
                alert.show();
            } 
            catch (IOException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText(bundle.getString("failed_to_load"));
                alert.show();
            } 
            catch (JsonParsingException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText(bundle.getString("invalid_json"));
                alert.show();
            }
        }
    }
    
    @FXML
    private void handleSave(ActionEvent event) {
        csgData.setCd_directory(cd_directory.getText());
        csgData.setCd_temp_dir(cd_temp_dir.getText());
        csgData.setCd_instr_name(cd_instr_name.getText());
        csgData.setCd_instr_home(cd_instr_home.getText());
        csgData.setCd_title(cd_title.getText());
        csgData.setStart_time(start_time.getPromptText());
        csgData.setEnd_time(end_time.getPromptText());
        csgData.setCd_style(cd_style.getPromptText());
        
        FileChooser fc = new FileChooser();
        File f = new File("work/");
        if (!f.isDirectory()) {
            if (!f.mkdir())
                f.mkdirs();
        }
        fc.setTitle("Save Workspace");
        fc.setInitialDirectory(f);
        File file = fc.showSaveDialog(workspace.getScene().getWindow());
        if (file != null) {
            try{
                fileController.handleSaveRequest(file.getPath() + ".json");
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText(bundle.getString("successful_save"));
                alert.show();
            }
            catch (IOException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText(bundle.getString("failed_to_save"));
                alert.show();
            }
        }
       
        
    }
    
    @FXML
    private void handleSaveAs(ActionEvent event) {
        csgData.setCd_directory(cd_directory.getText());
        csgData.setCd_temp_dir(cd_temp_dir.getText());
        csgData.setCd_instr_name(cd_instr_name.getText());
        csgData.setCd_instr_home(cd_instr_home.getText());
        csgData.setCd_title(cd_title.getText());
        csgData.setStart_time(start_time.getPromptText());
        csgData.setEnd_time(end_time.getPromptText());
        csgData.setCd_style(cd_style.getPromptText());
        
        FileChooser fc = new FileChooser();
        File f = new File("work/");
        if (!f.isDirectory()) {
            if (!f.mkdir())
                f.mkdirs();
        }
        fc.setTitle("Save Workspace");
        fc.setInitialDirectory(f);
        File file = fc.showSaveDialog(workspace.getScene().getWindow());
        if (file != null) {
            try{
                fileController.handleSaveRequest(file.getPath() + ".json");
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText(bundle.getString("successful_save"));
                alert.show();
            }
            catch (IOException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText(bundle.getString("failed_to_save"));
                alert.show();
            }
        }
    }
    
    @FXML
    private void handleExport(ActionEvent event) throws IOException {
        System.out.println("hey");
        FileUtils.deleteQuietly(toReplace);
        FileUtils.copyDirectory(toCopy, toExport);

	// SAVE IT TO A FILE
        fileController.handleSaveRequest(toReplace.getPath());
	// TELL THE USER THE FILE HAS BEEN SAVED
	AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        dialog.show(bundle.getString("exportCompleted"));
	// AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
	// THE APPROPRIATE CONTROLS
    
    }
    
    @FXML
    private void handleChangeExport(ActionEvent event){
        try {
	    fileController.handleSaveRequest("../CSGTester/public_html/js/SiteData.json");
            // PROMPT THE USER FOR A FILE NAME
            DirectoryChooser fc = new DirectoryChooser();
            fc.setInitialDirectory(new File("../CSGTester/export"));
            fc.setTitle("Export");

            File selectedDirectory = fc.showDialog(workspace.getScene().getWindow());
            if (selectedDirectory != null) {
                //export
                File publicHtml = new File("../CSGTester/public_html");
                File ohData = new File(selectedDirectory.getPath() + "/js/SiteData.json");
                toExport = selectedDirectory;
                toCopy = publicHtml;
                toReplace = ohData;
                cd_directory.setText(selectedDirectory.getPath());
                
            }
        } catch (IOException ioe) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(bundle.getString("exportError"));
        }
    }
    
    @FXML
    private void handleSelectExport(ActionEvent event){
        try {
	    fileController.handleSaveRequest("../CSGTester/public_html/js/SiteData.json");
            // PROMPT THE USER FOR A FILE NAME
            DirectoryChooser fc = new DirectoryChooser();
            fc.setInitialDirectory(new File("../CSGTester/templates"));
            fc.setTitle("Export Template");

            File selectedDirectory = fc.showDialog(workspace.getScene().getWindow());
            if (selectedDirectory != null) {
                //export
                //File publicHtml = new File("../CSGTester/public_html");
                //File ohData = new File(selectedDirectory.getPath() + "/js/SiteData.json");
                //toExport = selectedDirectory;
                //toCopy = publicHtml;
                //toReplace = ohData;
                cd_temp_dir.setText(selectedDirectory.getPath());
                
            }
        } catch (IOException ioe) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(bundle.getString("exportError"));
        }
    }
    
    @FXML
    private void handleExit(ActionEvent event) {
        Stage s = (Stage)workspace.getScene().getWindow();
        s.close();
    }
    
    @FXML
    private void handleCalendar(ActionEvent event){
        if(cal_start.getValue() == null || cal_end.getValue() == null){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(bundle.getString("emptyRec"));
        }
        String d1 = cal_start.getValue().toString();
        String d2 = cal_end.getValue().toString();
        System.out.println(d1);
        System.out.println(d2);
        String[] times1 = d1.split("-");
        String[] times2 = d2.split("-");
        if(Integer.parseInt(times1[0]) == Integer.parseInt(times2[0])){
            if(Integer.parseInt(times1[1]) == Integer.parseInt(times2[1])){
                if(Integer.parseInt(times1[2]) == Integer.parseInt(times2[2])){
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show(bundle.getString("invalidCal"));
                }
                else if(Integer.parseInt(times1[2]) > Integer.parseInt(times2[2])){
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show(bundle.getString("invalidCal"));
                }
                else{
                    return;
                }    
            }
            else if(Integer.parseInt(times1[1]) > Integer.parseInt(times2[1])){
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(bundle.getString("invalidCal"));
            }
            else{
                return;
            }
        }
        else if(Integer.parseInt(times1[0]) > Integer.parseInt(times2[0])){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(bundle.getString("invalidCal"));
        }
        else{
            return;
        }
    }
    
    @FXML 
    private void handleTADel(KeyEvent event){
        if(event.getCode().equals(KeyCode.DELETE)){
            Object selectedItem = ta_table.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                // GET THE TA AND REMOVE IT
                TeachingAssistant ta = (TeachingAssistant)selectedItem;
                String taName = ta.getName();
                
                jTPS_Transaction deletUR = new TAdeletUR(csgData, taName);
                jTPS.addTransaction(deletUR);
                ta_table.getItems().setAll(tas);
                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
                // WE'VE CHANGED STUFF
                //markWorkAsEdited();
                add = true;
                ta_add.setText("Add");
                updateAll();
            }
        }
    }
    
    @FXML 
    private void handleRecDel(KeyEvent event){
        if(event.getCode().equals(KeyCode.DELETE)){
            Object selectedItem = recitation_table.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                // GET THE TA AND REMOVE IT
                Recitation recit = (Recitation)selectedItem;
                //String taName = ta.getName();
                jTPS_Transaction deleteUR = new RecDeleteUR(recit, csgData);
                jTPS.addTransaction(deleteUR);
                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
                // WE'VE CHANGED STUFF
                //markWorkAsEdited();
                updateAll();
            }
        }
    }
    
    @FXML 
    private void handleSchDel(KeyEvent event){
        if(event.getCode().equals(KeyCode.DELETE)){
            Object selectedItem = sch_table.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                // GET THE TA AND REMOVE IT
                Schedule sch = (Schedule)selectedItem;
                //String taName = ta.getName();
                jTPS_Transaction deleteUR = new SchDeleteUR(sch, csgData);
                jTPS.addTransaction(deleteUR);
                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
                // WE'VE CHANGED STUFF
                //markWorkAsEdited();
                updateAll();
            }
        }
    }
    
    @FXML 
    private void handleProjDel(KeyEvent event){
        if(event.getCode().equals(KeyCode.DELETE)){
            Object selectedItem = proj_table.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                // GET THE TA AND REMOVE IT
                Project p = (Project)selectedItem;
                //String taName = ta.getName();
                jTPS_Transaction deleteUR = new ProjDeleteUR(p, csgData);
                jTPS.addTransaction(deleteUR);
                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
                // WE'VE CHANGED STUFF
                //markWorkAsEdited();
                updateAll();
            }
        }
    }
    
    @FXML 
    private void handleStudDel(KeyEvent event){
        if(event.getCode().equals(KeyCode.DELETE)){
            Object selectedItem = students_table.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                // GET THE TA AND REMOVE IT
                Student s = (Student)selectedItem;
                //String taName = ta.getName();
                jTPS_Transaction deleteUR = new StudDeleteUR(s, csgData);
                jTPS.addTransaction(deleteUR);
                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
                // WE'VE CHANGED STUFF
                //markWorkAsEdited();
                updateAll();
            }
        }
    }
    
    @FXML
    private void handleBanner(ActionEvent event) throws MalformedURLException{
        FileChooser fc = new FileChooser();
        File f = new File("../CSGTester/public_html/images");
        if (!f.isDirectory()) {
            if (!f.mkdir())
                f.mkdirs();
        }
        fc.setTitle("Select an Image");
        fc.setInitialDirectory(f);
        File file = fc.showOpenDialog(workspace.getScene().getWindow());
        if (file != null) {
            Image img = new Image(file.toURI().toURL().toExternalForm());
            ban_img.setImage(img);
            csgData.setBan_path(file.getAbsolutePath());
        }
    }
    
    @FXML
    private void handleLeft(ActionEvent event) throws MalformedURLException{
        FileChooser fc = new FileChooser();
        File f = new File("../CSGTester/public_html/images");
        if (!f.isDirectory()) {
            if (!f.mkdir())
                f.mkdirs();
        }
        fc.setTitle("Select an Image");
        fc.setInitialDirectory(f);
        File file = fc.showOpenDialog(workspace.getScene().getWindow());
        if (file != null) {
            Image img = new Image(file.toURI().toURL().toExternalForm());
            left_img.setImage(img);
            csgData.setLeft_path(file.getAbsolutePath());
        }
    }
    
    @FXML
    private void handleRight(ActionEvent event) throws MalformedURLException{
        FileChooser fc = new FileChooser();
        File f = new File("../CSGTester/public_html/images");
        if (!f.isDirectory()) {
            if (!f.mkdir())
                f.mkdirs();
        }
        fc.setTitle("Select an Image");
        fc.setInitialDirectory(f);
        File file = fc.showOpenDialog(workspace.getScene().getWindow());
        if (file != null) {
            Image img = new Image(file.toURI().toURL().toExternalForm());
            right_img.setImage(img);
            csgData.setRight_path(file.getAbsolutePath());
        }
    }
    
    void loadWorkspace() {
        cd_subject.setPromptText(csgData.getCd_subject());
        cd_number.setPromptText(csgData.getCd_number());
        cd_semester.setPromptText(csgData.getCd_semester());
        cd_year.setPromptText(csgData.getCd_year());
        cd_title.setText(csgData.getCd_title());
        cd_instr_name.setText(csgData.getCd_instr_name());
        cd_instr_home.setText(csgData.getCd_instr_home());
        cd_directory.setText(csgData.getCd_directory());
        cd_temp_dir.setText(csgData.getCd_temp_dir());
        cd_style.setPromptText(csgData.getCd_style());
        
        cal_start.setValue(LocalDate.of(2017, csgData.getCal_start_month(), csgData.getCal_start_day()));
        cal_end.setValue(LocalDate.of(2017, csgData.getCal_end_month(), csgData.getCal_end_day()));
        
        pages = csgData.getPages();
        cd_table.getItems().setAll(pages);
        
        tas = csgData.getTas();
        ta_table.getItems().setAll(tas);
        
        rec = csgData.getRec();
        recitation_table.getItems().setAll(rec);
        
        schedules = csgData.getSchedules();
        sch_table.getItems().setAll(schedules);
        
        project = csgData.getProjects();
        proj_table.getItems().setAll(project);
        
        students = csgData.getStudents();
        students_table.getItems().setAll(students);
    }

    private void clearSchForm() {
        type_in.setValue("");
        date_in.setValue(null);
        time_in.clear();
        title_in.clear();
        topic_in.clear();
        link_in.clear();
        criteria_in.clear();
    }

    private void clearStudForm() {
        stud_fn.setText("");
        stud_ln.setText("");
        stud_team.setValue(null);
        stud_role.setText("");
    }
}
