package controllers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.json.stream.JsonParsingException;
import view.AppMessageDialogSingleton;
import view.AppYesNoCancelDialogSingleton;

/**
 *
 * @author Justin Chiu
 */
public class CSGpromptController implements Initializable {
    
    @FXML
    private Button newButton;
    
    @FXML
    private AnchorPane rootPane;
    
    AppFileController controller;
    
    @FXML
    private void handleNewRequest(ActionEvent event) throws IOException {

        List<String> choices = new ArrayList<>();
        choices.add("English");
        choices.add("Japanese");


        ChoiceDialog<String> dialog = new ChoiceDialog<>("English", choices);
        dialog.setTitle("Choose a Language");
        dialog.setHeaderText("Select a language below");
        dialog.setContentText("Language:");

        // Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            String lang = result.get();
            if (lang.equals("English")){
                ResourceBundle bundle = ResourceBundle.getBundle("rsc.bundles.app_properties", Locale.getDefault());
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CSGWorkspace.fxml"), bundle);
                loader.setLocation(getClass().getResource("/view/CSGWorkspace.fxml"));
                Parent root = loader.load();
                Scene scene = new Scene(root);
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.setScene(scene);
                
                AppMessageDialogSingleton messageDialog = AppMessageDialogSingleton.getSingleton();
                messageDialog.init(app_stage);
                AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
                yesNoDialog.init(app_stage);
                
                app_stage.show();
            }
            else {
                ResourceBundle bundle = ResourceBundle.getBundle("rsc.bundles.app_properties_ja", Locale.JAPAN);
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CSGWorkspace.fxml"), bundle);
                loader.setLocation(getClass().getResource("/view/CSGWorkspace.fxml"));
                Parent root = loader.load();
                Scene scene = new Scene(root);
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.setScene(scene);
                app_stage.show();
            }
            
        }
    }
    
    @FXML
    private void handleLoadRequest(ActionEvent event) throws IOException {
        List<String> choices = new ArrayList<>();
        choices.add("English");
        choices.add("Japanese");


        ChoiceDialog<String> dialog = new ChoiceDialog<>("English", choices);
        dialog.setTitle("Choose a Language");
        dialog.setHeaderText("Select a language below");
        dialog.setContentText("Language:");

        // Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            String lang = result.get();
            if (lang.equals("English")){
                ResourceBundle bundle = ResourceBundle.getBundle("rsc.bundles.app_properties", Locale.getDefault());
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CSGWorkspace.fxml"), bundle);
                loader.setLocation(getClass().getResource("/view/CSGWorkspace.fxml"));
                Parent root = loader.load();
                Scene scene = new Scene(root);
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.setScene(scene);
                load(event, scene);
                app_stage.show();
            }
            else {
                ResourceBundle bundle = ResourceBundle.getBundle("rsc.bundles.app_properties_ja", Locale.JAPAN);
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CSGWorkspace.fxml"), bundle);
                loader.setLocation(getClass().getResource("/view/CSGWorkspace.fxml"));
                Parent root = loader.load();
                Scene scene = new Scene(root);
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.setScene(scene);
                load(event, scene);
                app_stage.show();
                
            }
            
        }
        
    }
    
    @FXML
    private void load(ActionEvent event, Scene scene) {
        FileChooser fc = new FileChooser();
        File f = new File("work");
        if (!f.isDirectory()) {
            if (!f.mkdir())
                f.mkdirs();
        }
        fc.setTitle("Load Workspace");
        fc.setInitialDirectory(f);
        File file = fc.showOpenDialog(scene.getWindow());
        if (file != null) {
            try {
                controller.handleLoadRequest(file.getPath());
                //loadWorkspace();
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText("successful load");
                alert.show();
            } 
            catch (IOException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("failed_to_load");
                alert.show();
            } 
            catch (JsonParsingException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("invalid_json");
                alert.show();
            }
        }
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    
    @FXML
    private void handleExit(ActionEvent event){
        Stage s = (Stage)rootPane.getScene().getWindow();
        s.close();
    }
    
}
