package controllers;

import data.CSGData;
import data.Page;
import data.Project;
import data.Recitation;
import data.Schedule;
import data.Student;
import data.TeachingAssistant;
import java.io.File;
import java.io.IOException;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.HashMap;
import javafx.stage.DirectoryChooser;
import javafx.stage.DirectoryChooserBuilder;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

/**
 * This class provides the event programmed responses for the file controls
 * that are provided by this framework.
 * 
 * @author Richard McKenna
 * @version 1.0
 */
public class AppFileController {

    // WE WANT TO KEEP TRACK OF WHEN SOMETHING HAS NOT BEEN SAVED
    boolean saved;
    CSGData data;
    // THIS IS THE FILE FOR THE WORK CURRENTLY BEING WORKED ON
    File currentWorkFile;

    /**
     * This constructor just keeps the app for later.
     * 
     * @param initApp The application within which this controller
     * will provide file toolbar responses.
     */
    public AppFileController(CSGData data) {
        // NOTHING YET
        this.data = data;

    }
    
    /**
     * This method marks the appropriate variable such that we know
     * that the current Work has been edited since it's been saved.
     * The UI is then updated to reflect this.
     * 
     * @param gui The user interface editing the Work.
     */
    public void markAsEdited() {
        // THE WORK IS NOW DIRTY
        saved = false;
        
    }

    /**
     * This method starts the process of editing new Work. If work is
     * already being edited, it will prompt the user to save it first.
     * 
     *
    public void handleNewRequest() {
	AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToMakeNew = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToMakeNew = promptToSave();
            }

            // IF THE USER REALLY WANTS TO MAKE A NEW COURSE
            if (continueToMakeNew) {
                // RESET THE WORKSPACE
		app.getWorkspaceComponent().resetWorkspace();

                // RESET THE DATA
                app.getDataComponent().resetData();
                
                // NOW RELOAD THE WORKSPACE WITH THE RESET DATA
                app.getWorkspaceComponent().reloadWorkspace(app.getDataComponent());

		// MAKE SURE THE WORKSPACE IS ACTIVATED
		app.getWorkspaceComponent().activateWorkspace(app.getGUI().getAppPane());
		
		// WORK IS NOT SAVED
                saved = false;
		currentWorkFile = null;

                // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                // THE APPROPRIATE CONTROLS
                app.getGUI().updateToolbarControls(saved);

                // TELL THE USER NEW WORK IS UNDERWAY
		dialog.show(props.getProperty(NEW_COMPLETED_TITLE), props.getProperty(NEW_COMPLETED_MESSAGE));
            }
        } catch (IOException ioe) {
            // SOMETHING WENT WRONG, PROVIDE FEEDBACK
	    dialog.show(props.getProperty(NEW_ERROR_TITLE), props.getProperty(NEW_ERROR_MESSAGE));
        }
    }
    */

    /**
     * This method lets the user open a Course saved to a file. It will also
     * make sure data for the current Course is not lost.
     * 
     * @param gui The user interface editing the course.
     */
    public void handleLoadRequest(String path) throws IOException{
        /*
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToOpen = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToOpen = promptToSave();
            }

            // IF THE USER REALLY WANTS TO OPEN A Course
            if (continueToOpen) {
                // GO AHEAD AND PROCEED LOADING A Course
                promptToOpen();
            }
        } catch (IOException ioe) {
            // SOMETHING WENT WRONG
	    ioe.printStackTrace();
        }
        */
        InputStream input = new FileInputStream(path);
        JsonReader reader = Json.createReader(input);
        JsonObject json = reader.readObject();
        input.close();
        reader.close();
        populateData(json);
        loadSite(json);
        loadTAs(json);
        loadRecitations(json);
        loadSchedule(json);
        loadProjects(json);
        loadStudents(json);
    }

    /**
     * This method will save the current course to a file. Note that we already
     * know the name of the file, so we won't need to prompt the user.
     * 
     * 
     * @param path
     */
    public void handleSaveRequest(String path) throws IOException {
        String subject = data.getCd_subject();
        String number = data.getCd_number();
        String semester = data.getCd_semester();
        String year = data.getCd_year();
        String title = data.getCd_title();
        String instr_name = data.getCd_instr_name();
        String instr_home = data.getCd_instr_home();
        String export_dir = data.getCd_directory();
        String template_dir = data.getCd_temp_dir();
        String start_time = data.getStart_time();
        String end_time = data.getEnd_time();
        String cd_style = data.getCd_style();
        int cal_start_month = data.getCal_start_month();
        int cal_start_day = data.getCal_start_day();
        int cal_end_month = data.getCal_end_month();
        int cal_end_day = data.getCal_end_day();
        String ban_path = data.getBan_path();
        String left_path = data.getLeft_path();
        String right_path = data.getRight_path();
        
        
        JsonArrayBuilder jsonBuilder = Json.createArrayBuilder();
        
        for(Page p: data.getPages()){
            JsonObject json = Json.createObjectBuilder()
                    .add("site_use", p.getUse())
                    .add("site_navbar_title", p.getNavbar())
                    .add("site_fileName", p.getFileName())
                    .add("site_script", p.getScript())
                    .build();
            jsonBuilder.add(json);
        }
        
        JsonArray site_pages = jsonBuilder.build();

        JsonArrayBuilder buildTA = Json.createArrayBuilder();
        
        for(TeachingAssistant t: data.getTas()){
            JsonObject json = Json.createObjectBuilder()
                    .add("undergrad_ta", t.getUndergrad())
                    .add("ta_name", t.getName())
                    .add("ta_email", t.getEmail())
                    .build();
            buildTA.add(json);
        }
        
        JsonArray ta_arr = buildTA.build();
        
        JsonArrayBuilder buildRecitations = Json.createArrayBuilder();
        
        for(Recitation r: data.getRec()){
            JsonObject json = Json.createObjectBuilder()
                    .add("col_section", r.getSection())
                    .add("col_instr", r.getInstructor())
                    .add("col_day_time", r.getDay_time())
                    .add("col_location", r.getLocation())
                    .add("col_ta1", r.getTa1().getName())
                    .add("col_ta2", r.getTa2().getName())
                    .build();
            buildRecitations.add(json);
        }
        
        JsonArray recitations = buildRecitations.build();
        
        JsonArrayBuilder buildSchedule = Json.createArrayBuilder();
        
        for(Schedule s: data.getSchedules()){
            JsonObject json = Json.createObjectBuilder()
                    .add("sch_type", s.getType())
                    .add("sch_date", s.getDate())
                    .add("sch_title", s.getTitle())
                    .add("sch_topic", s.getTopic())
                    .build();
            buildSchedule.add(json);
        }
        
        JsonArray schedule = buildSchedule.build();
        
        
        JsonArrayBuilder buildProject = Json.createArrayBuilder();
        
        for(Project p: data.getProjects()){
            JsonObject json = Json.createObjectBuilder()
                    .add("col_name", p.getName())
                    .add("col_color", p.getColor())
                    .add("col_text_color", p.getText_color())
                    .add("col_link", p.getLink())
                    .build();
            buildProject.add(json);
        }
        
        JsonArray project = buildProject.build();
        
        JsonArrayBuilder buildStudents = Json.createArrayBuilder();
        
        for(Student s: data.getStudents()){
            JsonObject json = Json.createObjectBuilder()
                    .add("first_name", s.getFirst())
                    .add("last_name", s.getLast())
                    .add("team_name", s.getTeam())
                    .add("role", s.getRole())
                    .build();
            buildStudents.add(json);
        }
        
        JsonArray students = buildStudents.build();
        
        JsonObject course_details = Json.createObjectBuilder()
                .add("cd_subject", subject)
                .add("cd_number", number)
                .add("cd_semester", semester)
                .add("cd_year", year)
                .add("cd_title", title)
                .add("cd_instr_name", instr_name)
                .add("cd_instr_home", instr_home)
                .add("cd_export_dir", export_dir)
                .add("cd_temp_dir", template_dir)
                .add("cd_style", cd_style)
                .add("site_pages", site_pages)
                .add("ta_table", ta_arr)
                .add("start_time", start_time)
                .add("end_time", end_time)
                .add("recitations", recitations)
                .add("schedule", schedule)
                .add("cal_start_month", cal_start_month)
                .add("cal_start_day", cal_start_day)
                .add("cal_end_month", cal_end_month)
                .add("cal_end_day", cal_end_day)
                .add("projects", project)
                .add("students", students)
                .add("ban_path", ban_path)
                .add("left_path", left_path)
                .add("right_path", right_path)
                .build();
            toJson(course_details, path);
        }

    private void toJson(JsonObject course_details, String path) throws FileNotFoundException {
        StringWriter str_writer = new StringWriter();
        HashMap<String, Object> jsonProp = new HashMap<>(1);
        jsonProp.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writer = Json.createWriterFactory(jsonProp);
        JsonWriter json_writer = writer.createWriter(str_writer);
        json_writer.writeObject(course_details);
        json_writer.close();
        OutputStream output = new FileOutputStream(path);
        JsonWriter json_to_file = Json.createWriter(output);
        String to_print = str_writer.toString();
        json_to_file.writeObject(course_details);
        PrintWriter print_writer =  new PrintWriter(path);
        print_writer.write(to_print);
        print_writer.close();
    }
    
    public void handleSaveAsRequest() {
        //PROMPT THE USER FOR A FILE NAME
        FileChooser fc = new FileChooser();
        /*
        fc.setInitialDirectory(new File(PATH_WORK));
        fc.setTitle(props.getProperty(SAVE_WORK_TITLE));
        fc.getExtensionFilters().addAll(
        new ExtensionFilter(props.getProperty(WORK_FILE_EXT_DESC), props.getProperty(WORK_FILE_EXT)));
        
        
        File selectedFile = fc.showSaveDialog(app.getGUI().getWindow());
        if (selectedFile != null) {
        saveWork(selectedFile);
        }
        */
    }
    
    public void handleExportRequest() {
        
        /*
        try {
            String newPath = "..\\CSGTester\\public_html\\js\\OfficeHoursGridData.json";
            File target = new File(newPath);
            saveWork(target);
        } catch (IOException ioe) {
	    ioe.printStackTrace();
        }
        DirectoryChooser dc = new DirectoryChooser();
        //dc.setInitialDirectory(new File(PATH_WORK));
        String path = dc.showDialog(null).getPath();
        System.out.println(path);
        String oldPath = "..\\CSGTester\\public_html";
        copy(oldPath, path);
        */
    }
    
    public void copy(String oldPath, String newPath) { 
        try { 
            (new File(newPath)).mkdirs();
            File old =new File(oldPath); 
            String[] file = old.list(); 
            File targget = null; 
            for (int i = 0; i < file.length; i++) { 
                if(oldPath.endsWith(File.separator))
                    targget = new File(oldPath+file[i]); 
                else
                    targget = new File(oldPath+File.separator+file[i]); 
                if(targget.isFile()){ 
                    FileInputStream input = new FileInputStream(targget); 
                    FileOutputStream output = new FileOutputStream(newPath + "/" + (targget.getName()).toString()); 
                    byte[] b = new byte[1024 * 5]; 
                    int templength; 
                    while ( (templength = input.read(b)) != -1)
                        output.write(b, 0, templength); 
                    output.flush(); 
                    output.close(); 
                    input.close(); 
                } 
                if(targget.isDirectory())
                    copy(oldPath+"/"+file[i],newPath+"/"+file[i]); 
            } 
        } 
        catch (Exception e) {  
            e.printStackTrace(); 
        } 
    }
    
    
    // HELPER METHOD FOR SAVING WORK
    private void saveWork(File selectedFile) throws IOException {
	// SAVE IT TO A FILE
	
	// MARK IT AS SAVED
	currentWorkFile = selectedFile;
	saved = true;
	
	// TELL THE USER THE FILE HAS BEEN SAVED
	
		    
	// AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
	// THE APPROPRIATE CONTROLS
    }
    
    /**
     * This method will exit the application, making sure the user doesn't lose
     * any data first.
     * 
     */
    public void handleExitRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToExit = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE
                continueToExit = promptToSave();
            }

            // IF THE USER REALLY WANTS TO EXIT THE APP
            if (continueToExit) {
                // EXIT THE APPLICATION
                System.exit(0);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * This helper method verifies that the user really wants to save their
     * unsaved work, which they might not want to do. Note that it could be used
     * in multiple contexts before doing other actions, like creating new
     * work, or opening another file. Note that the user will be
     * presented with 3 options: YES, NO, and CANCEL. YES means the user wants
     * to save their work and continue the other action (we return true to
     * denote this), NO means don't save the work but continue with the other
     * action (true is returned), CANCEL means don't save the work and don't
     * continue with the other action (false is returned).
     *
     * @return true if the user presses the YES option to save, true if the user
     * presses the NO option to not save, false if the user presses the CANCEL
     * option to not continue.
     */
    private boolean promptToSave() throws IOException {
	//PropertiesManager props = PropertiesManager.getPropertiesManager();
	
	// CHECK TO SEE IF THE CURRENT WORK HAS
	// BEEN SAVED AT LEAST ONCE
	
        // PROMPT THE USER TO SAVE UNSAVED WORK
	//AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
        //yesNoDialog.show(props.getProperty(SAVE_UNSAVED_WORK_TITLE), props.getProperty(SAVE_UNSAVED_WORK_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        //String selection = yesNoDialog.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        //if (selection.equals(AppYesNoCancelDialogSingleton.YES)) {
            // SAVE THE DATA FILE
            //AppDataComponent dataManager = app.getDataComponent();
	    
	    if (currentWorkFile == null) {
		// PROMPT THE USER FOR A FILE NAME
		FileChooser fc = new FileChooser();
		/*fc.setInitialDirectory(new File(PATH_WORK));
		fc.setTitle(props.getProperty(SAVE_WORK_TITLE));
		fc.getExtensionFilters().addAll(
		new ExtensionFilter(props.getProperty(WORK_FILE_EXT_DESC), props.getProperty(WORK_FILE_EXT)));

		File selectedFile = fc.showSaveDialog(app.getGUI().getWindow());
		if (selectedFile != null) {
		    saveWork(selectedFile);
		    saved = true;
		}
                */
	    }
	    else {
		saveWork(currentWorkFile);
		saved = true;
	    }
        // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        //else if (selection.equals(AppYesNoCancelDialogSingleton.CANCEL)) {
            //return false;
        

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }

    /**
     * This helper method asks the user for a file to open. The user-selected
     * file is then loaded and the GUI updated. Note that if the user cancels
     * the open process, nothing is done. If an error occurs loading the file, a
     * message is displayed, but nothing changes.
     */
    private void promptToOpen() {
	// WE'LL NEED TO GET CUSTOMIZED STUFF WITH THIS
	//PropertiesManager props = PropertiesManager.getPropertiesManager();
	
        // AND NOW ASK THE USER FOR THE FILE TO OPEN
        FileChooser fc = new FileChooser();
        /*fc.setInitialDirectory(new File(PATH_WORK));
	fc.setTitle(props.getProperty(LOAD_WORK_TITLE));
        File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
                // RESET THE WORKSPACE
		app.getWorkspaceComponent().resetWorkspace();

                // RESET THE DATA
                app.getDataComponent().resetData();
                
                // LOAD THE FILE INTO THE DATA
                app.getFileComponent().loadData(app.getDataComponent(), selectedFile.getAbsolutePath());
                
		// MAKE SURE THE WORKSPACE IS ACTIVATED
		app.getWorkspaceComponent().activateWorkspace(app.getGUI().getAppPane());
                
                // AND MAKE SURE THE FILE BUTTONS ARE PROPERLY ENABLED
                saved = true;
                app.getGUI().updateToolbarControls(saved);
                currentWorkFile = selectedFile;
            } catch (Exception e) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(LOAD_ERROR_TITLE), props.getProperty(LOAD_ERROR_MESSAGE));
            }
        }*/
    }

    /**
     * This mutator method marks the file as not saved, which means that when
     * the user wants to do a file-type operation, we should prompt the user to
     * save current work first. Note that this method should be called any time
     * the course is changed in some way.
     */
    public void markFileAsNotSaved() {
        saved = false;
    }

    /**
     * Accessor method for checking to see if the current work has been saved
     * since it was last edited.
     *
     * @return true if the current work is saved to the file, false otherwise.
     */
    public boolean isSaved() {
        return saved;
    }

    private void populateData(JsonObject json) {
        data.setCd_subject(json.getString("cd_subject"));
        data.setCd_number(json.getString("cd_number"));
        data.setCd_semester(json.getString("cd_semester"));
        data.setCd_year(json.getString("cd_year"));
        data.setCd_title(json.getString("cd_title"));
        data.setCd_instr_name(json.getString("cd_instr_name"));
        data.setCd_instr_home(json.getString("cd_instr_home"));
        data.setCd_directory(json.getString("cd_export_dir"));
        data.setCd_temp_dir(json.getString("cd_temp_dir"));
        data.setStart_time(json.getString("start_time"));
        data.setEnd_time(json.getString("end_time"));
        data.setCd_style(json.getString("cd_style"));
        data.setCal_start_month(json.getInt("cal_start_month"));
        data.setCal_start_day(json.getInt("cal_start_day"));
        data.setCal_end_month(json.getInt("cal_end_month"));
        data.setCal_end_day(json.getInt("cal_end_day"));
    }

    private void loadSite(JsonObject json) {
        data.clearSites();
        JsonArray arr = json.getJsonArray("site_pages");
        for (int i = 0; i < arr.size(); i++) {
            JsonObject site = arr.getJsonObject(i);
            boolean use = site.getBoolean("site_use");
            String navbar = site.getString("site_navbar_title");
            String fileName = site.getString("site_fileName");
            String script = site.getString("site_script");
            data.addSite(new Page(use, navbar, fileName, script));
        }
    }
    
    private void loadTAs(JsonObject json) {
        data.clearTAs();
        JsonArray arr = json.getJsonArray("ta_table");
        for (int i = 0; i < arr.size(); i++) {
            JsonObject site = arr.getJsonObject(i);
            boolean use = site.getBoolean("undergrad_ta");
            String navbar = site.getString("ta_name");
            String fileName = site.getString("ta_email");
            data.addTAs(new TeachingAssistant(navbar, fileName, use));
        }
    }

    private void loadRecitations(JsonObject json) {
        data.clearRecitations();
        JsonArray arr = json.getJsonArray("recitations");
        for (int i = 0; i < arr.size(); i++) {
            JsonObject site = arr.getJsonObject(i);
            String use = site.getString("col_section");
            String navbar = site.getString("col_instr");
            String fileName = site.getString("col_day_time");
            String script = site.getString("col_location");
            String ta1 = site.getString("col_ta1");
            String ta2 = site.getString("col_ta2");
            TeachingAssistant first_ta = data.getTA(ta1);
            TeachingAssistant second_ta = data.getTA(ta2);
            data.addRecitation(new Recitation(use, navbar, fileName, script, first_ta, second_ta));
        }
    }

    private void loadProjects(JsonObject json) {
        data.clearProjects();
        JsonArray arr = json.getJsonArray("projects");
        for (int i = 0; i < arr.size(); i++) {
            JsonObject site = arr.getJsonObject(i);
            String use = site.getString("col_name");
            String navbar = site.getString("col_color");
            String fileName = site.getString("col_text_color");
            String script = site.getString("col_link");
            data.addProject(new Project(use, navbar, fileName, script));
        }
    }

    private void loadSchedule(JsonObject json) {
        data.clearSchedules();
        JsonArray arr = json.getJsonArray("schedule");
        for (int i = 0; i < arr.size(); i++) {
            JsonObject site = arr.getJsonObject(i);
            String use = site.getString("sch_type");
            String navbar = site.getString("sch_date");
            String fileName = site.getString("sch_title");
            String script = site.getString("sch_topic");
            data.addSchedule(new Schedule(use, navbar, fileName, script));
        }
    }

    private void loadStudents(JsonObject json) {
        data.clearStudents();
        JsonArray arr = json.getJsonArray("students");
        for (int i = 0; i < arr.size(); i++) {
            JsonObject site = arr.getJsonObject(i);
            String use = site.getString("first_name");
            String navbar = site.getString("last_name");
            String fileName = site.getString("team_name");
            String script = site.getString("role");
            data.addStudent(new Student(use, navbar, fileName, script));
        }
    }
}
