/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Justin Chiu
 */
public class Student {
    private SimpleStringProperty first;
    private SimpleStringProperty last;
    private SimpleStringProperty team;
    private SimpleStringProperty role;
    
    public Student(String name, String color, String text_color, String link){
        this.first = new SimpleStringProperty(name);
        this.last = new SimpleStringProperty(color);
        this.team = new SimpleStringProperty(text_color);
        this.role = new SimpleStringProperty(link);
    }
    /**
     * @return the type
     */
    public String getFirst() {
        return first.get();
    }

    /**
     * @param type the type to set
     */
    public void setFirst(String type) {
        this.first.set(type);
    }

    /**
     * @return the date
     */
    public String getLast() {
        return last.get();
    }

    /**
     * @param date the date to set
     */
    public void setLast(String date) {
        this.last.set(date);
    }

    /**
     * @return the title
     */
    public String getTeam() {
        return team.get();
    }

    /**
     * @param title the title to set
     */
    public void setTeam(String title) {
        this.team.set(title);
    }

    /**
     * @return the topic
     */
    public String getRole() {
        return role.get();
    }

    /**
     * @param topic the topic to set
     */
    public void setRole(String topic) {
        this.role.set(topic);
    }
}
