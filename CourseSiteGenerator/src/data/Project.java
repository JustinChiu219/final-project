/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Justin Chiu
 */
public class Project {
    private SimpleStringProperty name;
    private SimpleStringProperty color;
    private SimpleStringProperty text_color;
    private SimpleStringProperty link;
    
    public Project(String name, String color, String text_color, String link){
        this.name = new SimpleStringProperty(name);
        this.color = new SimpleStringProperty(color);
        this.text_color = new SimpleStringProperty(text_color);
        this.link = new SimpleStringProperty(link);
    }

    /**
     * @return the type
     */
    public String getName() {
        return name.get();
    }

    /**
     * @param type the type to set
     */
    public void setName(String type) {
        this.name.set(type);
    }

    /**
     * @return the date
     */
    public String getColor() {
        return color.get();
    }

    /**
     * @param date the date to set
     */
    public void setColor(String date) {
        this.color.set(date);
    }

    /**
     * @return the title
     */
    public String getText_color() {
        return text_color.get();
    }

    /**
     * @param title the title to set
     */
    public void setText_color(String title) {
        this.text_color.set(title);
    }

    /**
     * @return the topic
     */
    public String getLink() {
        return link.get();
    }

    /**
     * @param topic the topic to set
     */
    public void setLink(String topic) {
        this.link.set(topic);
    }

    @Override
    public String toString() {
        return name.getValue();
    }
}
