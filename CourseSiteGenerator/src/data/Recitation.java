/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Justin Chiu
 */
public class Recitation {
    private SimpleStringProperty section;
    private SimpleStringProperty instructor;
    private SimpleStringProperty day_time;
    private SimpleStringProperty location;
    private TeachingAssistant ta1;
    private TeachingAssistant ta2;
    
    public Recitation(String section, String instructor, String day_time, String location, TeachingAssistant ta1, TeachingAssistant ta2){
        this.section = new SimpleStringProperty(section);
        this.instructor = new SimpleStringProperty(instructor);
        this.day_time = new SimpleStringProperty(day_time);
        this.location = new SimpleStringProperty(location);
        this.ta1 = ta1;
        this.ta2 = ta2;
    }

    /**
     * @return the section
     */
    public String getSection() {
        return section.get();
    }

    /**
     * @param section the section to set
     */
    public void setSection(String section) {
        this.section.set(section);
    }

    /**
     * @return the instructor
     */
    public String getInstructor() {
        return instructor.get();
    }

    /**
     * @param instructor the instructor to set
     */
    public void setInstructor(String instructor) {
        this.instructor.set(instructor);
    }

    /**
     * @return the day_time
     */
    public String getDay_time() {
        return day_time.get();
    }

    /**
     * @param day_time the day_time to set
     */
    public void setDay_time(String day_time) {
        this.day_time.set(day_time);
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location.get();
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location.set(location);
    }

    /**
     * @return the ta1
     */
    public TeachingAssistant getTa1() {
        return ta1;
    }

    /**
     * @param ta1 the ta1 to set
     */
    public void setTa1(TeachingAssistant ta1) {
        this.ta1 = ta1;
    }

    /**
     * @return the ta2
     */
    public TeachingAssistant getTa2() {
        return ta2;
    }

    /**
     * @param ta2 the ta2 to set
     */
    public void setTa2(TeachingAssistant ta2) {
        this.ta2 = ta2;
    }
}
