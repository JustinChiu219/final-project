/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Justin Chiu
 */
public class TeachingAssistant<E extends Comparable<E>> implements Comparable<E>  {
    // THE TABLE WILL STORE TA NAMES AND EMAILS
    private final SimpleStringProperty name;
    private final SimpleStringProperty email;
    private final SimpleBooleanProperty undergrad;
    
    public TeachingAssistant(String initName, String initEmail, boolean undergrad) {
        this.name = new SimpleStringProperty(initName);
        this.email = new SimpleStringProperty(initEmail);
        this.undergrad = new SimpleBooleanProperty(undergrad);
    }

    // ACCESSORS AND MUTATORS FOR THE PROPERTIES

    public String getName() {
        return name.get();
    }

    public void setName(String initName) {
        name.set(initName);
    }

    public String getEmail() {
        return email.get();
    }

    public void setEmail(String initEmail) {
        email.set(initEmail);
    }
    
    public boolean getUndergrad(){
        return undergrad.get();
    }
    
    public void setUndergrad(boolean undergrad){
        this.undergrad.set(undergrad);
    }

    @Override
    public int compareTo(E otherTA) {
        return getName().compareTo(((TeachingAssistant)otherTA).getName());
    }
    
    @Override
    public String toString() {
        return name.getValue();
    }
    
    
}


