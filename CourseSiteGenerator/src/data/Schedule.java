/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Justin Chiu
 */
public class Schedule {
    private SimpleStringProperty type;
    private SimpleStringProperty date;
    private SimpleStringProperty time;
    private SimpleStringProperty title;
    private SimpleStringProperty topic;
    private SimpleStringProperty link;
    private SimpleStringProperty criteria;
    
    public Schedule(String type, String date, String title, String topic){
        this.type = new SimpleStringProperty(type);
        this.date = new SimpleStringProperty(date);
        this.title = new SimpleStringProperty(title);
        this.topic = new SimpleStringProperty(topic);
    }

    public Schedule(String s_type, String s_date, String s_time, String s_title, String s_topic, String s_link, String s_criteria) {
        this.type = new SimpleStringProperty(s_type);
        this.date = new SimpleStringProperty(s_date);
        this.time = new SimpleStringProperty(s_time);
        this.title = new SimpleStringProperty(s_title);
        this.topic = new SimpleStringProperty(s_topic);
        this.link = new SimpleStringProperty(s_link);
        this.criteria = new SimpleStringProperty(s_criteria);
    }

    /**
     * @return the type
     */
    public String getType() {
        return type.get();
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type.set(type);
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date.get();
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date.set(date);
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title.get();
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title.set(title);
    }

    /**
     * @return the topic
     */
    public String getTopic() {
        return topic.get();
    }

    /**
     * @param topic the topic to set
     */
    public void setTopic(String topic) {
        this.topic.set(topic);
    }

    /**
     * @return the topic
     */
    public String getTime() {
        return time.get();
    }

    /**
     * @param topic the topic to set
     */
    public void setTime(String topic) {
        this.time.set(topic);
    }
    
    /**
     * @return the topic
     */
    public String getLink() {
        return link.get();
    }

    /**
     * @param topic the topic to set
     */
    public void setLink(String topic) {
        this.link.set(topic);
    }
    
    /**
     * @return the topic
     */
    public String getCriteria() {
        return criteria.get();
    }

    /**
     * @param topic the topic to set
     */
    public void setCriteria(String topic) {
        this.criteria.set(topic);
    }
}
