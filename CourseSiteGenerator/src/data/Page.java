/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Justin Chiu
 */
public class Page {
    private SimpleBooleanProperty use;
    private SimpleStringProperty navbar;
    private SimpleStringProperty fileName;
    private SimpleStringProperty script;
    
    public Page(boolean use, String navbar, String fileName, String script){
        this.use = new SimpleBooleanProperty(use);
        this.navbar = new SimpleStringProperty(navbar);
        this.fileName = new SimpleStringProperty(fileName);
        this.script = new SimpleStringProperty(script);
    }

    /**
     * @return the use
     */
    public boolean getUse() {
        return use.get();
    }

    /**
     * @param use the use to set
     */
    public void setUse(boolean use) {
        this.use.set(use);
    }

    /**
     * @return the navbar
     */
    public String getNavbar() {
        return navbar.get();
    }

    /**
     * @param navbar the navbar to set
     */
    public void setNavbar(String navbar) {
        this.navbar.set(navbar);
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName.get();
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName.set(fileName);
    }

    /**
     * @return the script
     */
    public String getScript() {
        return script.get();
    }

    /**
     * @param script the script to set
     */
    public void setScript(String script) {
        this.script.set(script);
    }
}
