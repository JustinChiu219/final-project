/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Justin Chiu
 */
public class CSGData {
    private SimpleStringProperty cd_semester;
    private SimpleStringProperty cd_title;
    private SimpleStringProperty cd_instr_name;
    private SimpleStringProperty cd_instr_home;
    private SimpleStringProperty cd_change;
    private SimpleStringProperty cd_number;
    private SimpleStringProperty cd_year;
    private SimpleStringProperty cd_table;
    private SimpleStringProperty cd_use_col;
    private SimpleStringProperty cd_navbar_col;
    private SimpleStringProperty cd_file_col;
    private SimpleStringProperty cd_script_col;
    private SimpleStringProperty cd_subject;
    private SimpleStringProperty cd_temp_dir;
    private SimpleStringProperty cd_style;
    private String cd_directory = "..\\\\..\\\\..\\\\Courses\\\\CSE219\\\\Summer2017\\\\public";
    private SimpleStringProperty start_time;
    private SimpleStringProperty end_time;
    private SimpleStringProperty ta_name;
    private SimpleStringProperty ta_email;
    
    private SimpleStringProperty rec_date_time;
    private SimpleStringProperty rec_loc;
    private SimpleStringProperty rec_instr;
    private SimpleStringProperty rec_section;
    private SimpleStringProperty rec_ta1;
    private SimpleStringProperty rec_ta2;
    
    private SimpleStringProperty title_in;
    private SimpleStringProperty topic_in;
    private SimpleStringProperty time_in;
    private SimpleStringProperty criteria_in;
    private SimpleStringProperty link_in;
    private SimpleStringProperty type_in;
    private SimpleStringProperty date_in;
    
    private int cal_start_day = 1;
    private int cal_start_month = 1;
    private int cal_end_day  = 1;
    private int cal_end_month = 1;
    
    
    private ObservableList<Page> pages;
    private ObservableList<Recitation> rec;
    private ObservableList<Schedule> schedules;
    private ObservableList<Project> projects;
    private ObservableList<Student> students;
    private ObservableList<TeachingAssistant> tas;
    
    private ObservableList<Student> studentsToAdd;
    
    private String ban_path;
    private String left_path;
    private String right_path;
    
    public CSGData(){
        cd_semester = new SimpleStringProperty("Fall");
        cd_title = new SimpleStringProperty("CSIII");
        cd_instr_name = new SimpleStringProperty("McKenna");
        cd_instr_home = new SimpleStringProperty("www.mckenna.com");
        cd_change = new SimpleStringProperty("test");
        cd_number = new SimpleStringProperty("219");
        cd_year = new SimpleStringProperty("2016");
        cd_table = new SimpleStringProperty("");
        cd_use_col = new SimpleStringProperty("");
        cd_navbar_col = new SimpleStringProperty("");
        cd_file_col = new SimpleStringProperty("");
        cd_script_col = new SimpleStringProperty("");
        cd_style = new SimpleStringProperty("style.css");
        cd_subject = new SimpleStringProperty("CSE");
        cd_temp_dir = new SimpleStringProperty("<insert template directory>");
        pages = FXCollections.observableArrayList();
        rec = FXCollections.observableArrayList();
        ta_name = new SimpleStringProperty("");
        ta_email = new SimpleStringProperty("");
        rec_date_time = new SimpleStringProperty("");
        rec_loc = new SimpleStringProperty("");
        rec_instr = new SimpleStringProperty("");
        rec_section = new SimpleStringProperty("");
        rec_ta1 = new SimpleStringProperty("");
        rec_ta2 = new SimpleStringProperty("");
        title_in = new SimpleStringProperty("");
        topic_in = new SimpleStringProperty("");
        time_in = new SimpleStringProperty("");
        criteria_in = new SimpleStringProperty("");
        link_in = new SimpleStringProperty("");
        type_in = new SimpleStringProperty("");
        date_in = new SimpleStringProperty("");
        schedules = FXCollections.observableArrayList();
        projects = FXCollections.observableArrayList();
        students = FXCollections.observableArrayList();
        start_time = new SimpleStringProperty("9");
        end_time = new SimpleStringProperty("13");
        tas = FXCollections.observableArrayList();
        studentsToAdd = FXCollections.observableArrayList();
    }

    public void addTA(String initName, String initEmail, boolean initUndergrad) {
        // MAKE THE TA
        TeachingAssistant ta = new TeachingAssistant(initName, initEmail, initUndergrad);

        // ADD THE TA
        if (!containsTA(initName, initEmail)) {
            tas.add(ta);
        }

        // SORT THE TAS
        Collections.sort(tas);
    }
    
    public void removeTA(String initName) {
        for (TeachingAssistant ta : tas) {
            if (initName.equals(ta.getName())) {
                tas.remove(ta);
                return;
            }
        }
    }
    
    public void removeRec(String section) {
        for (Recitation r : rec) {
            if (section.equals(r.getSection())) {
                rec.remove(r);
                return;
            }
        }
    }
    
    public void removeSch(String title) {
        for (Schedule s : schedules) {
            if (title.equals(s.getTitle())) {
                schedules.remove(s);
                return;
            }
        }
    }
    
    public void removeProj(String name) {
        for (Project p : projects) {
            if (name.equals(p.getName())) {
                projects.remove(p);
                return;
            }
        }
    }
    
    public void deleteAllStud(Project p){
        ArrayList<Student> listToRemove = new ArrayList<Student>();
        for(Student s : students){
            if(s.getTeam().equals(p.getName())){
                studentsToAdd.add(s);
                listToRemove.add(s);
            }
        }
        for(Student s : listToRemove){
            removeStud(s.getFirst(), s.getLast());
        }
    }
    
    public void addAllStud(Project p){
        ArrayList<Student> listToRemove = new ArrayList<Student>();
        for(Student s : studentsToAdd){
            students.add(s);
            listToRemove.add(s);
        }
        for(Student s : listToRemove){
            studentsToAdd.remove(s);
        }
        
    }
    
    public void removeStud(String first, String last) {
        for (Student s : students) {
            if (s.getFirst().equals(first) && s.getLast().equals(last)) {
                students.remove(s);
                return;
            }
        }
    }
    
    public Project getProject(String name) {
        for (Project p : projects) {
            if (p.getName().equals(name)) {
                return p;
            }
        }
        return null;
    }
    
    
    public TeachingAssistant getTA(String testName) {
        for (TeachingAssistant ta : tas) {
            if (ta.getName().equals(testName)) {
                return ta;
            }
        }
        return null;
    }
    
    public boolean containsTA(String testName, String testEmail) {
        for (TeachingAssistant ta : tas) {
            if (ta.getName().equals(testName)) {
                return true;
            }
            if (ta.getEmail().equals(testEmail)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean containsTAName(String testName) {
        for (TeachingAssistant ta : tas) {
            if (ta.getName().equals(testName)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean containsTAEmail(String testEmail) {
        for (TeachingAssistant ta : tas) {
            if (ta.getEmail().equals(testEmail)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean containsRecSection(String section) {
        for (Recitation r : rec) {
            if (section.equals(r.getSection())) {
                return true;
            }
        }
        return false;
    }
    
    public boolean containsSchTitle(String title) {
        for (Schedule s : schedules) {
            if (title.equals(s.getTitle())) {
                return true;
            }
        }
        return false;
    }
    
    public boolean containsProjName(String testName) {
        for (Project p : projects) {
            if (p.getName().equals(testName)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean containsStudName(String first, String last) {
        for (Student s : students) {
            if (s.getFirst().equals(first) && s.getLast().equals(last)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * @return the cd_semester
     */
    public String getCd_semester() {
        return cd_semester.get();
    }

    /**
     * @param cd_semester the cd_semester to set
     */
    public void setCd_semester(String cd_semester) {
        this.cd_semester.set(cd_semester);
    }

    /**
     * @return the cd_title
     */
    public String getCd_title() {
        return cd_title.get();
    }

    /**
     * @param cd_title the cd_title to set
     */
    public void setCd_title(String cd_title) {
        this.cd_title.set(cd_title);
    }

    /**
     * @return the cd_instr_name
     */
    public String getCd_instr_name() {
        return cd_instr_name.get();
    }

    /**
     * @param cd_instr_name the cd_instr_name to set
     */
    public void setCd_instr_name(String cd_instr_name) {
        this.cd_instr_name.set(cd_instr_name);
    }

    /**
     * @return the cd_instr_home
     */
    public String getCd_instr_home() {
        return cd_instr_home.get();
    }

    /**
     * @param cd_instr_home the cd_instr_home to set
     */
    public void setCd_instr_home(String cd_instr_home) {
        this.cd_instr_home.set(cd_instr_home);
    }

    /**
     * @return the cd_change
     */
    public String getCd_change() {
        return cd_change.get();
    }

    /**
     * @param cd_change the cd_change to set
     */
    public void setCd_change(String cd_change) {
        this.cd_change.set(cd_change);
    }

    /**
     * @return the cd_number
     */
    public String getCd_number() {
        return cd_number.get();
    }

    /**
     * @param cd_number the cd_number to set
     */
    public void setCd_number(String cd_number) {
        this.cd_number.set(cd_number);
    }

    /**
     * @return the cd_year
     */
    public String getCd_year() {
        return cd_year.get();
    }

    /**
     * @param cd_year the cd_year to set
     */
    public void setCd_year(String cd_year) {
        this.cd_year.set(cd_year);
    }

    /**
     * @return the cd_table
     */
    public String getCd_table() {
        return cd_table.get();
    }

    /**
     * @param cd_table the cd_table to set
     */
    public void setCd_table(String cd_table) {
        this.cd_table.set(cd_table);
    }

    /**
     * @return the cd_use_col
     */
    public String getCd_use_col() {
        return cd_use_col.get();
    }

    /**
     * @param cd_use_col the cd_use_col to set
     */
    public void setCd_use_col(String cd_use_col) {
        this.cd_use_col.set(cd_use_col);
    }

    /**
     * @return the cd_navbar_col
     */
    public String getCd_navbar_col() {
        return cd_navbar_col.get();
    }

    /**
     * @param cd_navbar_col the cd_navbar_col to set
     */
    public void setCd_navbar_col(String cd_navbar_col) {
        this.cd_navbar_col.set(cd_navbar_col);
    }

    /**
     * @return the cd_file_col
     */
    public String getCd_file_col() {
        return cd_file_col.get();
    }

    /**
     * @param cd_file_col the cd_file_col to set
     */
    public void setCd_file_col(String cd_file_col) {
        this.cd_file_col.set(cd_file_col);
    }

    /**
     * @return the cd_script_col
     */
    public String getCd_script_col() {
        return cd_script_col.get();
    }

    /**
     * @param cd_script_col the cd_script_col to set
     */
    public void setCd_script_col(String cd_script_col) {
        this.cd_script_col.set(cd_script_col);
    }

    /**
     * @return the cd_directory
     */
    public String getCd_directory() {
        return cd_directory;
    }

    /**
     * @param cd_directory the cd_directory to set
     */
    public void setCd_directory(String cd_directory) {
        this.cd_directory = cd_directory;
    }

    /**
     * @return the cd_subject
     */
    public String getCd_subject() {
        return cd_subject.get();
    }

    /**
     * @param cd_subject the cd_subject to set
     */
    public void setCd_subject(String cd_subject) {
        this.cd_subject.set(cd_subject);
    }

        /**
     * @return the cd_temp_dir
     */
    public String getCd_temp_dir() {
        return cd_temp_dir.get();
    }

    /**
     * @param cd_temp_dir the cd_temp_dir to set
     */
    public void setCd_temp_dir(String cd_temp_dir) {
        this.cd_temp_dir.set(cd_temp_dir);
    }

    /**
     * @return the pages
     */
    public ObservableList<Page> getPages() {
        return pages;
    }

    /**
     * @return the start_time
     */
    public String getStart_time() {
        return start_time.get();
    }

    /**
     * @param start_time the start_time to set
     */
    public void setStart_time(String start_time) {
        this.start_time.set(start_time);
    }

    /**
     * @return the end_time
     */
    public String getEnd_time() {
        return end_time.get();
    }

    /**
     * @param end_time the end_time to set
     */
    public void setEnd_time(String end_time) {
        this.end_time.set(end_time);
    }

    /**
     * @return the ta_name
     */
    public String getTa_name() {
        return ta_name.get();
    }

    /**
     * @param ta_name the ta_name to set
     */
    public void setTa_name(String ta_name) {
        this.ta_name.set(ta_name);
    }

    /**
     * @return the ta_email
     */
    public String getTa_email() {
        return ta_email.get();
    }

    /**
     * @param ta_email the ta_email to set
     */
    public void setTa_email(String ta_email) {
        this.ta_email.set(ta_email);
    }

    /**
     * @return the rec_date_time
     */
    public String getRec_date_time() {
        return rec_date_time.get();
    }

    /**
     * @param rec_date_time the rec_date_time to set
     */
    public void setRec_date_time(String rec_date_time) {
        this.rec_date_time.set(rec_date_time);
    }

    /**
     * @return the rec_loc
     */
    public String getRec_loc() {
        return rec_loc.get();
    }

    /**
     * @param rec_loc the rec_loc to set
     */
    public void setRec_loc(String rec_loc) {
        this.rec_loc.set(rec_loc);
    }

    /**
     * @return the rec_instr
     */
    public String getRec_instr() {
        return rec_instr.get();
    }

    /**
     * @param rec_instr the rec_instr to set
     */
    public void setRec_instr(String rec_instr) {
        this.rec_instr.set(rec_instr);
    }

    /**
     * @return the rec_section
     */
    public String getRec_section() {
        return rec_section.get();
    }

    /**
     * @param rec_section the rec_section to set
     */
    public void setRec_section(String rec_section) {
        this.rec_section.set(rec_section);
    }

    /**
     * @return the rec_ta1
     */
    public String getRec_ta1() {
        return rec_ta1.get();
    }

    /**
     * @param rec_ta1 the rec_ta1 to set
     */
    public void setRec_ta1(String rec_ta1) {
        this.rec_ta1.set(rec_ta1);
    }

    /**
     * @return the rec_ta2
     */
    public String getRec_ta2() {
        return rec_ta2.get();
    }

    /**
     * @param rec_ta2 the rec_ta2 to set
     */
    public void setRec_ta2(String rec_ta2) {
        this.rec_ta2.set(rec_ta2);
    }

    /**
     * @return the rec
     */
    public ObservableList<Recitation> getRec() {
        return rec;
    }

    /**
     * @return the schedule
     */
    public ObservableList<Schedule> getSchedules() {
        return schedules;
    }

    /**
     * @return the projects
     */
    public ObservableList<Project> getProjects() {
        return projects;
    }

    /**
     * @return the students
     */
    public ObservableList<Student> getStudents() {
        return students;
    }
    
    public void addSite(Page p){
        pages.add(p);
    }
    
    public void clearSites(){
        pages.removeAll(pages);
    }
    
    public void clearRecitations(){
        rec.removeAll(rec);
    }
    
    public void clearSchedules(){
        schedules.removeAll(schedules);
    }
    
    public void clearProjects(){
        projects.removeAll(projects);
    }
    
    public void clearStudents(){
        students.removeAll(students);
    }
    
    public void addRecitation(Recitation r){
        rec.add(r);
    }
    
    public void addSchedule(Schedule s){
        schedules.add(s);
    }
    
    public void addProject(Project p){
        projects.add(p);
    }
    
    public void addStudent(Student s){
        students.add(s);
    }

    public void removeRecitation(Recitation r){
        rec.remove(r);
    }
    
    public void removeSchedule(Schedule s){
        schedules.remove(s);
    }
    
    public void removeProject(Project p){
        projects.remove(p);
    }
    
    public void removeStudent(Student s){
        students.remove(s);
    }
    
    /**
     * @return the tas
     */
    public ObservableList<TeachingAssistant> getTas() {
        return tas;
    }
    
    public void clearTAs(){
        tas.removeAll(tas);
    }
    
    public void addTAs(TeachingAssistant ta){
        tas.add(ta);
    }

    /**
     * @return the cd_style
     */
    public String getCd_style() {
        return cd_style.get();
    }

    /**
     * @param cd_style the cd_style to set
     */
    public void setCd_style(String cd_style) {
        this.cd_style.set(cd_style);
    }

    /**
     * @return the cal_end_day
     */
    public int getCal_end_day() {
        return cal_end_day;
    }

    /**
     * @param cal_end_day the cal_end_day to set
     */
    public void setCal_end_day(int cal_end_day) {
        this.cal_end_day = cal_end_day;
    }

    /**
     * @return the cal_end_month
     */
    public int getCal_end_month() {
        return cal_end_month;
    }

    /**
     * @param cal_end_month the cal_end_month to set
     */
    public void setCal_end_month(int cal_end_month) {
        this.cal_end_month = cal_end_month;
    }

    /**
     * @return the cal_start_day
     */
    public int getCal_start_day() {
        return cal_start_day;
    }

    /**
     * @param cal_start_day the cal_start_day to set
     */
    public void setCal_start_day(int cal_start_day) {
        this.cal_start_day = cal_start_day;
    }

    /**
     * @return the cal_start_month
     */
    public int getCal_start_month() {
        return cal_start_month;
    }

    /**
     * @param cal_start_month the cal_start_month to set
     */
    public void setCal_start_month(int cal_start_month) {
        this.cal_start_month = cal_start_month;
    }

    /**
     * @return the ban_path
     */
    public String getBan_path() {
        return ban_path;
    }

    /**
     * @param ban_path the ban_path to set
     */
    public void setBan_path(String ban_path) {
        this.ban_path = ban_path;
    }

    /**
     * @return the left_path
     */
    public String getLeft_path() {
        return left_path;
    }

    /**
     * @param left_path the left_path to set
     */
    public void setLeft_path(String left_path) {
        this.left_path = left_path;
    }

    /**
     * @return the right_path
     */
    public String getRight_path() {
        return right_path;
    }

    /**
     * @param right_path the right_path to set
     */
    public void setRight_path(String right_path) {
        this.right_path = right_path;
    }
    
}
