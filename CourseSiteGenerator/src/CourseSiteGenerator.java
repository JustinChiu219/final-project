

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.application.Application.launch;
import javafx.scene.image.Image;


/**
 *
 * @author Justin Chiu
 */
public class CourseSiteGenerator extends Application {
    private static final String app_icon = "rsc/images/CSGLogo.png";
    /**
     * This is where program execution begins. Since this is a JavaFX app it
     * will simply call launch, which gets JavaFX rolling, resulting in sending
     * the properly initialized Stage (i.e. window) to the start method inherited
     * from AppTemplate, defined in the Desktop Java Framework.
     * @throws java.lang.Exception
     */
    
    @Override
    public void start(Stage primaryStage) throws Exception{
        
        /*
	// LET'S START BY INITIALIZING OUR DIALOGS
	AppMessageDialogSingleton messageDialog = AppMessageDialogSingleton.getSingleton();
	messageDialog.init(primaryStage);
	AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
	yesNoDialog.init(primaryStage);
	PropertiesManager props = PropertiesManager.getPropertiesManager();
        */
        // GET THE TITLE FROM THE XML FILE
        //String appTitle = props.getProperty(APP_TITLE);
        //String appIcon = props.getProperty(APP_LOGO);
        // BUILD THE APP GUI OBJECT FIRST, BUT DON'T
        Parent root = FXMLLoader.load(getClass().getResource("view/CSGprompt.fxml"));
        primaryStage.setTitle("Course Site Generator");
        primaryStage.setScene(new Scene(root));
        URL url = this.getClass().getClassLoader().getResource(app_icon);
        String path = url.toExternalForm();
        Image icon = new Image(path);
        primaryStage.getIcons().add(icon);
        primaryStage.show();
    }
    
    public static void main(String[] args) {
	launch(args);
    }
}
