/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import data.CSGData;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author user
 */
public class AppFileControllerTest {
    
    public AppFileControllerTest() {
    }
    

    /**
     * Test of handleLoadRequest method, of class AppFileController.
     */
    @Test
    public void testHandleLoadRequest() throws Exception {
        System.out.println("handleLoadRequest");
        String path = "./work/SiteSaveTest.json";
        CSGData data = new CSGData(); 
        AppFileController instance = new AppFileController(data);
        instance.handleLoadRequest(path);
        
        assertEquals("CSE", data.getCd_subject());
        assertEquals("219", data.getCd_number());
        assertEquals("Fall", data.getCd_semester());
        assertEquals("2016", data.getCd_year());
        assertEquals("CSIII", data.getCd_title());
        assertEquals("McKenna", data.getCd_instr_name());
        assertEquals("www.mckenna.com", data.getCd_instr_home());
        assertEquals("..\\\\..\\\\..\\\\Courses\\\\CSE219\\\\Summer2017\\\\public", data.getCd_directory());
        assertEquals("<insert template directory>", data.getCd_temp_dir());
        assertEquals("style.css", data.getCd_style());
        
        
        //site pages
        assertTrue(data.getPages().get(0).getUse());
        assertEquals("Home", data.getPages().get(0).getNavbar());
        assertEquals("index.html", data.getPages().get(0).getFileName());
        assertEquals("HomeBuilder.js", data.getPages().get(0).getScript());
        
        assertTrue(data.getPages().get(1).getUse());
        assertEquals("Syllabus", data.getPages().get(1).getNavbar());
        assertEquals("syllabus.html", data.getPages().get(1).getFileName());
        assertEquals("SyllabusBuilder.js", data.getPages().get(1).getScript());
        
        assertTrue(data.getPages().get(2).getUse());
        assertEquals("Schedule", data.getPages().get(2).getNavbar());
        assertEquals("schedule.html", data.getPages().get(2).getFileName());
        assertEquals("ScheduleBuilder.js", data.getPages().get(2).getScript());
        
        assertTrue(data.getPages().get(3).getUse());
        assertEquals("HWs", data.getPages().get(3).getNavbar());
        assertEquals("hws.html", data.getPages().get(3).getFileName());
        assertEquals("HWsBuilder.js", data.getPages().get(3).getScript());
        
        assertFalse(data.getPages().get(4).getUse());
        assertEquals("Projects", data.getPages().get(4).getNavbar());
        assertEquals("projects.html", data.getPages().get(4).getFileName());
        assertEquals("ProjectsBuilder.js", data.getPages().get(4).getScript());
        
        //ta_table
        assertTrue(data.getTas().get(0).getUndergrad());
        assertEquals("Kevin Li", data.getTas().get(0).getName());
        assertEquals("kevin.li@stonybrook.edu", data.getTas().get(0).getEmail());
        
        assertFalse(data.getTas().get(1).getUndergrad());
        assertEquals("Lauren Cheong", data.getTas().get(1).getName());
        assertEquals("lauren.cheong@stonybrook.edu", data.getTas().get(1).getEmail());
        
        //start/end time
        assertEquals("9", data.getStart_time());
        assertEquals("13", data.getEnd_time());
        
        //recitations
        assertEquals("R02", data.getRec().get(0).getSection());
        assertEquals("McKenna", data.getRec().get(0).getInstructor());
        assertEquals("Wed 3:30pm-4:23pm", data.getRec().get(0).getDay_time());
        assertEquals("Old CS 2114", data.getRec().get(0).getLocation());
        assertEquals("Kevin Li", data.getRec().get(0).getTa1().getName());
        assertEquals("Lauren Cheong", data.getRec().get(0).getTa2().getName());
        
        assertEquals("R05", data.getRec().get(1).getSection());
        assertEquals("Banerjee", data.getRec().get(1).getInstructor());
        assertEquals("Tues 5:30pm-6:23pm", data.getRec().get(1).getDay_time());
        assertEquals("Old CS 2114", data.getRec().get(1).getLocation());
        assertEquals("Kevin Li", data.getRec().get(1).getTa1().getName());
        assertEquals("Lauren Cheong", data.getRec().get(1).getTa2().getName());
        
        //schedule
        assertEquals("Holiday", data.getSchedules().get(0).getType());
        assertEquals("2/9/2017", data.getSchedules().get(0).getDate());
        assertEquals("SNOW DAY", data.getSchedules().get(0).getTitle());
        assertEquals("Day off", data.getSchedules().get(0).getTopic());
        
        assertEquals("Lecture", data.getSchedules().get(1).getType());
        assertEquals("2/14/2017", data.getSchedules().get(1).getDate());
        assertEquals("Lecture 3", data.getSchedules().get(1).getTitle());
        assertEquals("Event Programming", data.getSchedules().get(1).getTopic());
        
        assertEquals("Holiday", data.getSchedules().get(2).getType());
        assertEquals("3/13/2017", data.getSchedules().get(2).getDate());
        assertEquals("Spring Break", data.getSchedules().get(2).getTitle());
        assertEquals("Day off", data.getSchedules().get(2).getTopic());
        
        assertEquals("HW", data.getSchedules().get(3).getType());
        assertEquals("3/27/2017", data.getSchedules().get(3).getDate());
        assertEquals("HW3", data.getSchedules().get(3).getTitle());
        assertEquals("UML", data.getSchedules().get(3).getTopic());
        
        //projects
        assertEquals("Atomic Comics", data.getProjects().get(0).getName());
        assertEquals("552211", data.getProjects().get(0).getColor());
        assertEquals("ffffff", data.getProjects().get(0).getText_color());
        assertEquals("http://atomicomic.com", data.getProjects().get(0).getLink());
        
        assertEquals("C4 Comics", data.getProjects().get(1).getName());
        assertEquals("235399", data.getProjects().get(1).getColor());
        assertEquals("ffffff", data.getProjects().get(1).getText_color());
        assertEquals("https://c4-comics.appspot.com", data.getProjects().get(1).getLink());
        
        //students
        assertEquals("Beau", data.getStudents().get(0).getFirst());
        assertEquals("Brummell", data.getStudents().get(0).getLast());
        assertEquals("Atomic Comics", data.getStudents().get(0).getTeam());
        assertEquals("Lead Designer", data.getStudents().get(0).getRole());
        
        assertEquals("Jane", data.getStudents().get(1).getFirst());
        assertEquals("Doe", data.getStudents().get(1).getLast());
        assertEquals("C4 Comics", data.getStudents().get(1).getTeam());
        assertEquals("Lead Programmer", data.getStudents().get(1).getRole());
        
        assertEquals("Noonian", data.getStudents().get(2).getFirst());
        assertEquals("Soong", data.getStudents().get(2).getLast());
        assertEquals("Atomic Comics", data.getStudents().get(2).getTeam());
        assertEquals("Data Designer", data.getStudents().get(2).getRole());
        
        
        
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

   
}
