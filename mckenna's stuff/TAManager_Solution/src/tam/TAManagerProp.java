package tam;

/**
 * This enum provides a list of all the user interface
 * text that needs to be loaded from the XML properties
 * file. By simply changing the XML file we could initialize
 * this application such that all UI controls are provided
 * in another language.
 * 
 * @author Richard McKenna
 * @version 1.0
 */
public enum TAManagerProp {
    // FOR SIMPLE OK/CANCEL DIALOG BOXES
    OK_PROMPT,
    CANCEL_PROMPT,
    
    // THESE ARE FOR TEXT PARTICULAR TO THE APP'S WORKSPACE CONTROLS
    TAS_HEADER_TEXT,
    NAME_COLUMN_TEXT,
    EMAIL_COLUMN_TEXT,
    NAME_PROMPT_TEXT,
    EMAIL_PROMPT_TEXT,
    ADD_BUTTON_TEXT,
    UPDATE_BUTTON_TEXT,
    CLEAR_BUTTON_TEXT,
    OFFICE_HOURS_SUBHEADER,
    OFFICE_HOURS_TABLE_HEADERS,
    DAYS_OF_WEEK,
    
    // THESE ARE FOR ERROR MESSAGES PARTICULAR TO THE APP
    MISSING_TA_NAME_TITLE,
    MISSING_TA_NAME_MESSAGE,
    MISSING_TA_EMAIL_TITLE,
    MISSING_TA_EMAIL_MESSAGE,
    TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE,
    TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE,
    TA_EMAIL_INVALID_TITLE,
    TA_EMAIL_INVALID_MESSAGE,
    OFFICE_HOURS_REMOVED_TITLE,
    OFFICE_HOURS_REMOVED_MESSAGE,
    START_OVER_END_TITLE,
    START_OVER_END_MESSAGE,
    
    TIME_12AM,
    TIME_1AM,
    TIME_2AM,
    TIME_3AM,
    TIME_4AM,
    TIME_5AM,
    TIME_6AM,
    TIME_7AM,
    TIME_8AM,
    TIME_9AM,
    TIME_10AM,
    TIME_11AM,
    TIME_12PM,
    TIME_1PM,
    TIME_2PM,
    TIME_3PM,
    TIME_4PM,
    TIME_5PM,
    TIME_6PM,
    TIME_7PM,
    TIME_8PM,
    TIME_9PM,
    TIME_10PM,
    TIME_11PM
}
