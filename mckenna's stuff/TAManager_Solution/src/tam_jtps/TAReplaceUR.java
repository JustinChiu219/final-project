/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam_jtps;

import javafx.scene.control.TableView;
import jtps.jTPS_Transaction;
import tam.TAManagerApp;
import tam_data.TAData;
import tam_data.TeachingAssistant;
import tam_workspace.TAWorkspace;

/**
 *
 * @author zhaotingyi
 */
public class TAReplaceUR implements jTPS_Transaction{
    private String TAname;
    private String TAemail;
    private String newName;
    private String newEmail;
    private TAManagerApp app;
    private TAData data;
    
    public TAReplaceUR(TAManagerApp app){
        this.app = app;
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        data = (TAData)app.getDataComponent();
        newName = workspace.getNameTextField().getText();
        newEmail = workspace.getEmailTextField().getText();
        TableView taTable = workspace.getTATable();
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        TeachingAssistant ta = (TeachingAssistant)selectedItem;
        TAname = ta.getName();
        TAemail = ta.getEmail();
    }

    @Override
    public void doTransaction() {
        data.replaceTAName(TAname, newName);
        data.removeTA(TAname);
        data.addTA(newName, newEmail);
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        taTable.getSelectionModel().select(data.getTA(newName));
    }

    @Override
    public void undoTransaction() {
        data.replaceTAName(newName, TAname);
        data.removeTA(newName);
        data.addTA(TAname, TAemail);
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        taTable.getSelectionModel().select(data.getTA(TAname));
    }
    
    
    
}
