// DATA TO LOAD
var startHour;
var endHour;
var daysOfWeek;
var officeHours;
var undergradTAs;

function buildOfficeHoursGrid() {
    var dataFile = "./js/SiteData.json";
    loadData(dataFile, loadOfficeHours);
}

function loadData(jsonFile, callback) {
    $.getJSON(jsonFile, function(json) {
        callback(json);
    });
}

function loadOfficeHours(json) {
    addUndergradTAs(json);
}

function initDays(data) {
    // GET THE START AND END HOURS
    startHour = parseInt(data.start_time);
    endHour = parseInt(data.end_time);

    // THEN MAKE THE TIMES
    daysOfWeek = new Array();
    daysOfWeek[0] = "MONDAY";
    daysOfWeek[1] = "TUESDAY";
    daysOfWeek[2] = "WEDNESDAY";
    daysOfWeek[3] = "THURSDAY";
    daysOfWeek[4] = "FRIDAY";
}

function addUndergradTAs(data) {
    var tas = $("#ta_table");
    var tasPerRow = 4;
    var numTAs = data.ta_table.length;
    for (var i = 0; i < data.ta_table.length; ) {
        var text = "";
        text = "<tr>";
        for (var j = 0; j < tasPerRow; j++) {
            text += buildTACell(i, numTAs, data.ta_table[i]);
            i++;
        }
        text += "</tr>";
        tas.append(text);
    }
}
function buildTACell(counter, numTAs, ta) {
    if (counter >= numTAs)
        return "<td></td>";

    var name = ta.ta_name;
    var abbrName = name.replace(/\s/g, '');
    var email = ta.ta_email;
    var undergrad = ta.undergrad_ta;
    var text = "<td class='tas'><img width='100' height='100'"
                + " src='./images/tas/" + abbrName + ".JPG' "
                + " alt='" + name + "' /><br />"
                + "<strong>" + name + "</strong><br />"
                + "<span class='email'>" + email + "</span><br />"
                + "<br />" + "Undergrad: " + undergrad + "<br /></td>";
    return text;
}
